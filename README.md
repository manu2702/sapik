# SAPIK #

This is a webapp using the Spring framework.

### What is this repository for? ###

I created this app for a school project during my studies at CNAM. It is an attempt to merge french doctors' data files with the help of [Apache Jena's libs](https://jena.apache.org/) and provide easy requests on the doctors database depending on the condition and type of specialty of the doctors

#### Version : 1.0.0 ####

### How do I get set up? ###

Launch the jar :
* java -jar -Xmx2048m sapik-web-app-1.0.0-SNAPSHOT.jar