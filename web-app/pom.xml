<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>zea</groupId>
		<artifactId>sapik</artifactId>
		<version>1.0.0</version>
	</parent>
	<artifactId>sapik-web-app</artifactId>
	<name>Sapik web app</name>
	<packaging>jar</packaging>

	<properties>
		<!-- Support de JodaTime dans Hibernate -->
		<joda-time-hibernate.version>1.4</joda-time-hibernate.version>
		<usertype-core.version>5.0.0.GA</usertype-core.version>

		<!-- Utilisation de CodeHale - Metrics -->
		<metrics-spring.version>3.1.2</metrics-spring.version>

		<!-- Exposition auto API avec Swagger -->
		<version.lib.springfox-swagger2>2.5.0</version.lib.springfox-swagger2>

		<!-- Plugins -->
		<version.plugin.maven-assembly>2.5.2</version.plugin.maven-assembly>
		<version.plugin.appassembler>1.10</version.plugin.appassembler>
		<version.plugin.spring-boot-maven>1.4.1.RELEASE</version.plugin.spring-boot-maven>
		<version.lib.commons-io>2.4</version.lib.commons-io>
		<version.lib.commons-lang3>3.4</version.lib.commons-lang3>
		<jena.version>3.0.1</jena.version>
		<jena.fuseki.version>2.3.1</jena.fuseki.version>

	</properties>


	<dependencies>

		<dependency>
			<groupId>com.mashape.unirest</groupId>
			<artifactId>unirest-java</artifactId>
			<version>1.4.5</version>
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpcore</artifactId>
			<version>4.4.1</version><!--$NO-MVN-MAN-VER$ -->
		</dependency>
		<dependency>
			<groupId>org.apache.httpcomponents</groupId>
			<artifactId>httpclient</artifactId>
			<version>4.4.1</version><!--$NO-MVN-MAN-VER$ -->
		</dependency>
		<!-- IIF packaging == war : scope = provided! -->
		<!-- <dependency> -->
		<!-- <groupId>org.springframework.boot</groupId> -->
		<!-- <artifactId>spring-boot-starter-tomcat</artifactId> -->
		<!-- <scope>provided</scope> -->
		<!-- </dependency> -->

		<dependency>
			<!-- slf4j, {jul,log4j}-over-slf4j, logback -->
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-logging</artifactId>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
				<exclusion>
					<artifactId>jcl-over-slf4j</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
				<exclusion>
					<groupId>ch.qos.logback</groupId>
					<artifactId>logback-classic</artifactId>
				</exclusion>
				<exclusion>
					<groupId>ch.qos.logback</groupId>
					<artifactId>logback-core</artifactId>
				</exclusion>
				<exclusion>
					<artifactId>slf4j-log4j12</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>jul-to-slf4j</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>log4j-over-slf4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- SPRING BOOT starters -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-actuator</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<!-- <exclusions> -->
			<!-- <exclusion> -->
			<!-- <groupId>org.jboss.logging</groupId> -->
			<!-- <artifactId>jboss-logging</artifactId> -->
			<!-- </exclusion> -->
			<!-- </exclusions> -->
		</dependency>
		<dependency>
			<!-- generate "spring-configuration-metadata.json" from @ConfigurationProperties -->
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-configuration-processor</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.batch</groupId>
			<artifactId>spring-batch-core</artifactId>
		</dependency>

		<!-- API: -->

		<!-- LIBS: Spring -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context-support</artifactId>
		</dependency>

		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time-hibernate</artifactId>
			<version>${joda-time-hibernate.version}</version>
		</dependency>
		<dependency>
			<groupId>org.jadira.usertype</groupId>
			<artifactId>usertype.core</artifactId>
			<version>${usertype-core.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
				<exclusion>
					<groupId>org.jboss.logging</groupId>
					<artifactId>jboss-logging</artifactId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- LIBS: -->
		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-joda</artifactId>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-hibernate4</artifactId>
		</dependency>

		<!-- LIBS : documentation API -->
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
			<version>${version.lib.springfox-swagger2}</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
			<version>${version.lib.springfox-swagger2}</version>
		</dependency>

		<!-- LIBS : monitoring -->
		<dependency>
			<groupId>io.dropwizard.metrics</groupId>
			<artifactId>metrics-core</artifactId>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>io.dropwizard.metrics</groupId>
			<artifactId>metrics-annotation</artifactId>
			<version>${dropwizard-metrics.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>io.dropwizard.metrics</groupId>
			<artifactId>metrics-healthchecks</artifactId>
			<version>${dropwizard-metrics.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>io.dropwizard.metrics</groupId>
			<artifactId>metrics-json</artifactId>
			<version>${dropwizard-metrics.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>io.dropwizard.metrics</groupId>
			<artifactId>metrics-jvm</artifactId>
			<version>${dropwizard-metrics.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>io.dropwizard.metrics</groupId>
			<artifactId>metrics-servlet</artifactId>
			<version>${dropwizard-metrics.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>io.dropwizard.metrics</groupId>
			<artifactId>metrics-servlets</artifactId>
			<exclusions>
				<exclusion>
					<artifactId>metrics-healthchecks</artifactId>
					<groupId>io.dropwizard.metrics</groupId>
				</exclusion>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>com.ryantenney.metrics</groupId>
			<artifactId>metrics-spring</artifactId>
			<version>${metrics-spring.version}</version>
			<exclusions>
				<exclusion>
					<groupId>com.codahale.metrics</groupId>
					<artifactId>metrics-annotation</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.codahale.metrics</groupId>
					<artifactId>metrics-core</artifactId>
				</exclusion>
				<exclusion>
					<groupId>com.codahale.metrics</groupId>
					<artifactId>metrics-healthchecks</artifactId>
				</exclusion>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>

		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>${version.lib.commons-io}</version>
			<exclusions></exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>${version.lib.commons-lang3}</version>
		</dependency>

		<!-- TESTS: -->
		<!-- Common testing libs are already configured in parent POM -->

		<dependency>
			<groupId>com.jayway.jsonpath</groupId>
			<artifactId>json-path</artifactId>
			<scope>test</scope>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-api</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
			</exclusions>
		</dependency>

		<!-- Apache Jena -->
		<dependency>
			<groupId>org.apache.jena</groupId>
			<artifactId>jena-csv</artifactId>
			<version>${jena.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-log4j12</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
				<exclusion>
					<artifactId>log4j</artifactId>
					<groupId>log4j</groupId>
				</exclusion>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-api</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>jcl-over-slf4j</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>org.apache.jena</groupId>
			<artifactId>jena-fuseki-server</artifactId>
			<version>${jena.fuseki.version}</version>
			<exclusions>
				<exclusion>
					<artifactId>slf4j-log4j12</artifactId>
					<groupId>org.slf4j</groupId>
				</exclusion>
				<exclusion>
					<artifactId>log4j</artifactId>
					<groupId>log4j</groupId>
				</exclusion>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>jcl-over-slf4j</artifactId>
				</exclusion>
				<exclusion>
					<groupId>org.slf4j</groupId>
					<artifactId>slf4j-api</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
	</dependencies>

	<build>
		<resources>
			<resource>
				<directory>${basedir}/src/main/resources</directory>
				<filtering>true</filtering>
				<includes>
					<include>**/application*.properties</include>
					<include>**/application*.yml</include>
				</includes>
			</resource>
			<resource>
				<directory>${basedir}/src/main/resources</directory>
				<excludes>
					<exclude>**/application*.properties</exclude>
					<exclude>**/application*.yml</exclude>
				</excludes>
			</resource>
			<resource>
				<directory>${basedir}/src/main/webapp</directory>
				<excludes>
					<exclude>*.csv</exclude>
				</excludes>
				<includes>
					<include>*.css</include>
					<include>*.js</include>
					<include>*.map</include>
					<include>*.eot</include>
					<include>*.svg</include>
					<include>*.ttf</include>
					<include>*.woff</include>
					<include>*.otf</include>
					<include>*.eot</include>
					<include>*.geojson</include>
					<include>*.md</include>
					<include>*.png</include>
					<include>*.gif</include>
					<include>*.ico</include>
					<include>*.tpl</include>
					<include>*.ttl</include>
					<include>*.xml</include>
					<include>*.html</include>
				</includes>
			</resource>
		</resources>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<mainClass>com.sapik.SapikWebApplication</mainClass>
					<jvmArguments>
						-Xmx2048m
					</jvmArguments>
				</configuration>
			</plugin>
		</plugins>
		<pluginManagement>
			<plugins>
				<!--This plugin's configuration is used to store Eclipse m2e settings 
					only. It has no influence on the Maven build itself. -->
				<plugin>
					<groupId>org.eclipse.m2e</groupId>
					<artifactId>lifecycle-mapping</artifactId>
					<version>1.0.0</version>
					<configuration>
						<lifecycleMappingMetadata>
							<pluginExecutions>
								<pluginExecution>
									<pluginExecutionFilter>
										<groupId>
											org.codehaus.mojo
										</groupId>
										<artifactId>
											build-helper-maven-plugin
										</artifactId>
										<versionRange>
											[1.9.1,)
										</versionRange>
										<goals>
											<goal>add-source</goal>
										</goals>
									</pluginExecutionFilter>
									<action>
										<ignore />
									</action>
								</pluginExecution>
							</pluginExecutions>
						</lifecycleMappingMetadata>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-compiler-plugin</artifactId>
					<configuration>
						<source>1.8</source>
						<target>1.8</target>
					</configuration>
				</plugin>
				<plugin>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-maven-plugin</artifactId>
				</plugin>
			</plugins>
		</pluginManagement>
	</build>

	<profiles>
		<!-- ======= DEV ====== -->
		<profile>
			<id>dev</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<properties>
				<logback.loglevel>INFO</logback.loglevel>
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.springframework.boot</groupId>
						<artifactId>spring-boot-maven-plugin</artifactId>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>prod</id>
			<properties>
				<!-- log configuration -->
				<logback.loglevel>INFO</logback.loglevel>
			</properties>
			<build>
				<plugins>
					<plugin>
						<groupId>org.springframework.boot</groupId>
						<artifactId>spring-boot-maven-plugin</artifactId>
						<configuration>
							<arguments>
								<argument>spring.profiles.active=prod</argument>
							</arguments>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>
</project>