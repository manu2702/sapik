package com.sapik.webapp.domain;

import java.util.List;

public interface IFusekiConnector {

	/**
	 * Method for sending a model and store it into fuseki
	 * 
	 * @param model
	 * @return true if succeeded
	 */
	public boolean storeCsv(String path, String encoding, String elementNamespace);

	/**
	 * Overrides the method for sending a model and store it into fuseki to match the etalab's file
	 * 
	 * @param model
	 * @return true if succeeded
	 */
	public boolean storeCsv(String path, String encoding, String elementNamespace, String categories, String colTitles);

	/**
	 * Method for getting the illnesses corresponding to a symptom
	 * 
	 * @param symptom
	 * @return a list of possible illnesses
	 */
	public List<String> getIllnesses(String symptom);

	/**
	 * Method for getting all doctors data
	 * 
	 * @return all doctors
	 */
	public String getAll(String url);

	/**
	 * Method for getting a particular doctor
	 * 
	 * @param id
	 * @return the Doctor resource in json
	 */
	public String get(String url, String id);

	/**
	 * Gets the institutions in a coordinates-delimited area
	 * 
	 * @param xMin
	 * @param xMax
	 * @param yMin
	 * @param yMax
	 * @return the list of institutions in JSON
	 */
	public String getInstitutionsInArea(String xMin, String xMax, String yMin, String yMax);

	/**
	 * Gets the institutions in a coordinates-delimited area
	 * 
	 * @param spec
	 * @param xMin
	 * @param xMax
	 * @param yMin
	 * @param yMax
	 * @return the list of institutions in JSON
	 */
	public String getInstitutionsInAreaForSpec(String spec, String xMin, String xMax, String yMin, String yMax);
}
