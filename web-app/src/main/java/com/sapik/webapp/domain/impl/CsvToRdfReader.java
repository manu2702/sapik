package com.sapik.webapp.domain.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;

public class CsvToRdfReader {

	private static final Logger LOGGER = LoggerFactory.getLogger(CsvToRdfReader.class);

	public CsvToRdfReader() {
//		LOGGER.debug("Initializing CSV2RDF ...");
		//CSV2RDF.init();
	}

	public Model parseCsv(String path, String hostname, String predicate, String port) {
		try {
			Model model = getModelFromCsv(path, hostname, predicate, port);// RDFDataMgr.loadModel(path);
			return model;
		} catch (Exception e) {
			LOGGER.debug("Problem parsing the CSV file : \n{}", e);
		}
		return null;
	}

	public Model parseCsv(String path, String hostname, String predicate, String port, String categories, String colTitles) {
		try {
			Model model = getModelFromCsv(path, hostname, predicate, port, categories, colTitles);// RDFDataMgr.loadModel(path);
			return model;
		} catch (Exception e) {
			LOGGER.debug("Problem parsing the CSV file : \n{}", e);
		}
		return null;
	}

	private Model getModelFromCsv(String csvFilePath, String hostname, String predicate, String port) {
		Model m = ModelFactory.createDefaultModel();
		int id = 1;
		int linecount = 0;
		

		// CSV column delimiter
		String delimiter = ",";

		String csvURI;
		String doctorNS;
		if(predicate.contains("cilit")) {
			csvURI = "http://".concat(hostname).concat(":").concat(port).concat("/").concat(predicate).concat("s").trim();
			m.setNsPrefix(predicate.trim().concat("s"), csvURI.concat("/"));
			predicate = "facility";//predicate.substring(0, predicate.indexOf("tie")+1).concat("y");
		} else {
			csvURI = "http://".concat(hostname).concat(":").concat(port).concat("/").concat(predicate).concat("s").trim();
			m.setNsPrefix(predicate.trim().concat("s"), csvURI.concat("/"));
		}
		doctorNS = csvURI.concat("#".concat(predicate.trim()));
		
		m.setNsPrefix(predicate.trim(), doctorNS.concat("/"));

		try {

			List<String> lines = Files.readAllLines(Paths.get(csvFilePath), Charset.forName("UTF-8"));

			List<String> colListing = new ArrayList<String>();
			Map<String, Property> properties = new HashMap<>();
			for (String line : lines) {
				StringTokenizer tokenizer = new StringTokenizer(line, delimiter);
				linecount++;

				if (linecount == 1) {
					while (tokenizer.hasMoreTokens()) {

						String colname = tokenizer.nextToken();

						String colURI = doctorNS.concat("/").concat(colname.trim().toLowerCase().replace(" ", "-"));

						colListing.add(colURI);

						Property p = m.createProperty(doctorNS.concat("/"), colname.trim().toLowerCase().replace(" ", "-"));
						
						properties.put(colURI, p);
					}
				} else {
					
					int i = 0;

					while (tokenizer.hasMoreTokens()) {
						String colvalue = tokenizer.nextToken();
						String col = null;
						if (colvalue.startsWith("\"") && !colvalue.endsWith("\"")) {
							StringBuilder prop = new StringBuilder(colvalue);
							colvalue = tokenizer.nextToken();
							while (!colvalue.endsWith("\"")) {
								prop.append(colvalue);
								if (tokenizer.hasMoreTokens())
									colvalue = tokenizer.nextToken();
								else
									break;
							}
							if (colvalue.contains("\""))
								prop.append(colvalue);
							colvalue = prop.toString().replace("\\\"", " ")
												      .replaceAll("\\\\\"", "")
													  .replace("\"", " ")
													  .replace("''", " ").trim();
						} else if(predicate.equals("facility") && i == 1){
							while(!NumberUtils.isNumber(col)) {
								col = tokenizer.nextToken();
								if(colvalue != null) {
									if(col != null){
										colvalue = colvalue.concat(col);
										colvalue = colvalue.replace('É', 'E');
									}
								} else {
									colvalue = col;
								}
							}
						}
						if (i == 9)
							LOGGER.warn("Problem with line : {}", line);

						if(colvalue == "null") {
							LOGGER.debug("Got null value");
						} else {
							Resource r = m.createResource(csvURI.concat("/").concat(Integer.toString(id)),
									m.createResource(csvURI.concat("#".concat(predicate.trim()))));
							try {
								r.addProperty(properties.get(colListing.get(i)), colvalue);
							} catch (IndexOutOfBoundsException e) {
								LOGGER.warn("colvalue = {}", colvalue);
								LOGGER.warn("line = {}", line);
							}
						}
						if(col != null && NumberUtils.isNumber(col)) {
							i++;
							Resource r = m.createResource(csvURI.concat("/").concat(Integer.toString(id)),
									m.createResource(csvURI.concat("#".concat(predicate.trim()))));
							try {
								r.addProperty(properties.get(colListing.get(i)), col);
							} catch (IndexOutOfBoundsException e) {
								LOGGER.warn("colvalue = {}", col);
								LOGGER.warn("line = {}", line);
							}
						}
						
						i++;

					}
					
					id++;
				}
			}
		} catch (IOException e) {
			LOGGER.warn("Error during parsing : {}", e);
		}

		return m;
	}

	private Model getModelFromCsv(String csvFilePath, String hostname, String predicate, String port, String categories, String colTitles) {
		Model m = ModelFactory.createDefaultModel();
		int id = 1;
		int linecount = 0;
		

		// CSV column delimiter
		String delimiter = ",";

		String csvURI;
		String mainNS;
		csvURI = "http://".concat(hostname).concat(":").concat(port).concat("/").concat(predicate).concat("s").trim();
		m.setNsPrefix(predicate.trim().concat("s"), csvURI.concat("/"));

		try {

			List<String> lines = Files.readAllLines(Paths.get(csvFilePath), Charset.forName("UTF-8"));

			//StringTokenizer catToken = new StringTokenizer(categories, delimiter);
			
			Map<String, List<String>> fileModel = getFileMapping(colTitles);
			

			
			//List<String> colListing = new ArrayList<String>();
			Map<String, Property> properties = new HashMap<>();
			//Map<String, Property> mainProperties = new HashMap<>();
			
			for (String line : lines) {
				StringTokenizer tokenizer = new StringTokenizer(line, delimiter);
				linecount++;
				String colURI;
				if (linecount == 1) {
					
					//mainNS = csvURI.concat("#");
					mainNS = csvURI.concat("#".concat(predicate.trim()).concat("/"));

					//m.setNsPrefix(predicate.trim(), mainNS.concat("/"));
					
					for(Map.Entry<String, List<String>> entry : fileModel.entrySet()) {
						colURI = mainNS.concat(entry.getKey().trim().toLowerCase());
						m.setNsPrefix(entry.getKey(), colURI.concat("/"));
						for(String cat : entry.getValue()) {
							if(cat.equals("nofinesset"))
								properties.put(entry.getKey().substring(0, 1).concat(cat), m.createProperty(colURI.concat("/").concat(cat)));
							else
								properties.put(cat, m.createProperty(colURI.concat("/").concat(cat)));
						}
					}
				} else {
					int i = 0;
					String mainCol = null;
					Resource r;
					List<String> propList = new ArrayList<>();
					while (tokenizer.hasMoreTokens()) {
						String colvalue = tokenizer.nextToken();
						if(colvalue.contains("habilité aide sociale")) {
							StringTokenizer tmpToken = tokenizer;
							if(tmpToken.nextToken().contains("recours PUI")) {
								colvalue = colvalue.concat(" ").concat(tokenizer.nextToken());
								tmpToken = null;
							}
						}
						if (colvalue.startsWith("\"") && !colvalue.endsWith("\"")) {
							StringBuilder prop = new StringBuilder(colvalue);
							colvalue = tokenizer.nextToken();
							while (!colvalue.endsWith("\"")) {
								prop.append(colvalue);
								if (tokenizer.hasMoreTokens())
									colvalue = tokenizer.nextToken();
								else
									break;
							}
							if (colvalue.contains("\""))
								prop.append(colvalue);
							colvalue = prop.toString().replace("\\\"", " ")
												      .replaceAll("\\\\\"", "")
													  .replace("\"", " ")
													  .replace("''", " ").trim();
						}
						if(colvalue == "null") {
							LOGGER.debug("Got null value");
						} else if(i == 0) {
							mainCol = colvalue;
						} else {
							if(mainCol.equals("geolocalisation")){
								if(colvalue == null || colvalue == "null") {
									while(tokenizer.hasMoreTokens()) {
										colvalue = tokenizer.nextToken();
									}
									break;
								} else if(i==3) {
									try {
									colvalue.concat(", ").concat(tokenizer.nextToken())
									.concat(", ").concat(tokenizer.nextToken())
									.concat(", ").concat(tokenizer.nextToken())
									.concat(", ").concat(tokenizer.nextToken())
									.concat(", ").concat(tokenizer.nextToken())
									.concat(", ").concat(tokenizer.nextToken());
									} catch (NoSuchElementException e) {
										//LOGGER.warn("Token doesn't exist! line nb {} : {}", linecount, line);
										break;
									}
								}
							}
							r = m.createResource(csvURI.concat("/").concat(Integer.toString(id)),
									m.createResource(csvURI.concat("#".concat(predicate.trim()).concat("/").concat(mainCol))));/*.concat(predicate.trim()).concat("/")*/
							propList.add(colvalue);
							try {
								if(i-1<30) {
									String propertyStr = fileModel.get(mainCol).get(i-1);
										r.addProperty(properties.get(propertyStr), colvalue);
									//}
								}
							} catch (IndexOutOfBoundsException e) {
								LOGGER.warn("Trying to get property {} from key {} with i-1={}", fileModel.get(mainCol).get(i-1), mainCol, i-1);
								LOGGER.warn("colvalue = {}", colvalue);
								LOGGER.warn("line = {}", line);
							}
						}
						i++;
					}
					id++;
				}
			}
		} catch (IOException e) {
			LOGGER.warn("Error during parsing : {}", e);
		}

		return m;
	}

	private Map<String, List<String>> getFileMapping(String titles) {
		Map<String, List<String>> mapping = new HashMap<String, List<String>>();
		StringTokenizer catTitles = new StringTokenizer(titles, ",");
		while (catTitles.hasMoreTokens()) {
			String actual = catTitles.nextToken();
			StringTokenizer title = new StringTokenizer(actual, "-");
			String cat = title.nextToken();
			if (mapping.containsKey(cat) && title.hasMoreTokens()) {
				String prop = title.nextToken();
				if(prop.contains("finesset"))
					prop = cat.substring(0, 1).concat(prop);
				mapping.get(cat).add(prop);
			} else if (title.hasMoreTokens()){
				String prop = title.nextToken();
				if(prop.contains("finesset"))
					prop = cat.substring(0, 1).concat(prop);
				List<String> tit = new ArrayList<>();
				tit.add(prop);
				mapping.put(cat, tit);
			}
		}
		return mapping;
	}
}