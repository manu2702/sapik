package com.sapik.webapp.domain.impl;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

import com.sapik.webapp.config.AppSettings;
import com.sapik.webapp.domain.ICsvRepository;
import com.sapik.webapp.services.SapikWebappService;

@Component
@DependsOn("csvFileProcessJob")
public class CsvRepository implements ICsvRepository, ResourceLoaderAware, ServletContextAware {

	private static final Logger LOGGER = LoggerFactory.getLogger(CsvRepository.class);

	@SuppressWarnings("unused")
	private ResourceLoader resourceLoader;

	@Autowired
	private SapikWebappService service;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private AppSettings settings;

	@Autowired
	protected FusekiEmbeddedServer fuseki;

	@Override
	public Thread getCsvFile(final String fileUrl, final String namespace) {
		Thread thread = new Thread() {
			@Override
			public void run() {
				String fileToDownloadUrl = getFileUrl(fileUrl);
				String file = getFile(fileToDownloadUrl);
				try {
					while (!ping("127.0.0.1", 3030)) {
						Thread.sleep(1000);
					}
				} catch (IOException e) {
					LOGGER.warn("IOException on doctors file treatment thread :\n{}", e.getMessage());
				} catch (InterruptedException e) {
					LOGGER.warn("InterruptedException on doctors file treatment thread :\n{}", e.getMessage());
				}
				service.saveFile(file, "ISO-8859-1", fileToDownloadUrl, namespace);
			}
		};
		thread.start();
		return thread;
	}

	@Override
	public Thread getCsvFile(final String fileUrl, final String namespace, final String categories,
			final String colTitles) {
		Thread thread = new Thread() {
			@Override
			public void run() {
				String file = getFile(fileUrl);
				try {
					while (!ping("127.0.0.1", 3030)) {
						Thread.sleep(1000);
					}
				} catch (IOException e) {
					LOGGER.warn("IOException on doctors file treatment thread :\n{}", e.getMessage());
				} catch (InterruptedException e) {
					LOGGER.warn("InterruptedException on doctors file treatment thread :\n{}", e.getMessage());
				}
				service.saveFile(file, "ISO-8859-1", fileUrl, namespace, categories, colTitles);
			}
		};
		thread.start();
		return thread;
	}

	@DependsOn("fusekiEmbeddedServer")
	@Override
	public void setResourceLoader(ResourceLoader resourceLoader) {
		// LOGGER.info("Setting resource loader for CsvRepository");
		this.resourceLoader = resourceLoader;
		Thread doctorsThread = null;
		Thread institutionsThread = null;
		// LOGGER.info("Getting doctors CSV file as resource ...");
		for (String predicate : settings.getCsvPredicates()) {
			if (predicate.equals("institutions")) {
				institutionsThread = getCsvFile(settings.getCsvUrls().get(settings.getCsvPredicates().indexOf(predicate)),
						settings.getCsvNamespaces().get(settings.getCsvPredicates().indexOf(predicate)),
						settings.getCsvEtalabCategories(), settings.getEtalabDataTitles());
				// LOGGER.info("Found institutions file");
			} else {
				doctorsThread = getCsvFile(settings.getCsvUrls().get(settings.getCsvPredicates().indexOf(predicate)),
						settings.getCsvNamespaces().get(settings.getCsvPredicates().indexOf(predicate)));
			}
		}
		setDataLoadedAlert(doctorsThread, institutionsThread);
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		LOGGER.info("Setting Servlet context to CsvRepository");
		this.servletContext = servletContext;
	}

	private void setDataLoadedAlert(Thread t1, Thread t2) {
		Thread thread = new Thread() {
			@Override
			public void run() {
				while(t1.isAlive() || t2.isAlive() || !service.dataLoaded()) {
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						LOGGER.warn("Couldn't pause data loaded utility thread.");
					}
				}
				LOGGER.info(new StringBuilder("\n")//
						.append("***************************************************************************\n")//
						.append("* ----------------------------------------------------------------------- *\n")//
						.append("* !! Data loaded into Fuseki - SUCCESS                                 !! *\n")//
						.append("* !! You can now submit requests to the server and use the application !! *\n")//
						.append("* !! All services READY                                                !! *\n")//
						.append("* ----------------------------------------------------------------------- *\n")//
						.append("***************************************************************************")//
						.toString());
			}
		};
		thread.start();
	}

	private boolean ping(String ip, int port) throws IOException {
		boolean response = false;
		Socket socket = new Socket();
		try {
			SocketAddress addr = new InetSocketAddress(ip, port);
			socket.connect(addr, 10);
			response = socket.isConnected();
		} catch (IOException e) {
//			LOGGER.info("Unknown host {} Stack error :", ip, e.getStackTrace());
			socket.close();
			return false;
		}
		socket.close();
		return response;
	}

	private String getFileUrl(String url) {
		LOGGER.info("Getting file link from : {}", url);

		URL website;

		List<String> lines = null;

		String uploadsDir = File.separator.concat("uploads").concat(File.separator);

		String realPathtoUploads = servletContext.getRealPath(uploadsDir);
		if (!new File(realPathtoUploads).exists())
			new File(realPathtoUploads).mkdir();

		String newFile = "tempFile" + Long.toString(Math.round(Math.random() * 10)) + ".txt";// url.substring(url.lastIndexOf("/"));
		File f = new File(realPathtoUploads + File.separator + newFile);

		try {
			website = new URL(url);
			FileUtils.copyURLToFile(website, f);
			lines = Files.readAllLines(Paths.get(f.getAbsolutePath()), Charset.forName("ISO-8859-1"));
		} catch (IOException e) {
			LOGGER.warn("Problem while getting html page {}\n{}", url, e);
		}
		for (String line : lines) {
			if (line.contains("http-equiv") && line.contains("URL")) {
				String fileUrl = line.substring(line.indexOf("/upload/"), line.lastIndexOf(".csv"));
				url = "url:http://www.has-sante.fr/portail".concat(fileUrl).concat(".csv");
				// LOGGER.info("File to download URL : {}", url);
				f.delete();
				return url;
			}
		}
		return url;
	}

	private String getFile(String url) {
		LOGGER.info("Getting file from : {}", url);

		URL website;

		String uploadsDir = File.separator.concat("uploads").concat(File.separator);

		String newFile = null;

		String realPathtoUploads = servletContext.getRealPath(uploadsDir);
		if (!new File(realPathtoUploads).exists())
			new File(realPathtoUploads).mkdir();

		String[] l = url.split("/");
		newFile = l[l.length - 1] == "" ? l[l.length - 2] : l[l.length - 1];
		File f = new File(realPathtoUploads.concat(File.separator).concat(newFile));

		try {
			website = new URL(url);
			FileUtils.copyURLToFile(website, f);
			return realPathtoUploads.concat(File.separator).concat(newFile);
		} catch (IOException e) {
			LOGGER.warn("Problem while getting CSV file {}\n{}", url, e);
		}
		return null;
	}
}
