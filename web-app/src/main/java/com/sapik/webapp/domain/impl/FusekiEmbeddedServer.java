package com.sapik.webapp.domain.impl;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.jena.fuseki.cmd.FusekiCmd;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class FusekiEmbeddedServer extends Thread {

	private String[] args;
	private String[] predicates;

	private static final Logger LOGGER = LoggerFactory.getLogger(FusekiEmbeddedServer.class);

	public FusekiEmbeddedServer(String[] args, String[] predicates) {
		this.args = args;
		this.predicates = predicates;
	}

	public FusekiEmbeddedServer() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		configTdbInstances(predicates, Paths.get(".").toAbsolutePath().normalize().toString());
		FusekiCmd.main(args);
	}

	public boolean isStarted() throws IOException {
		if (ping("127.0.0.1", 3030)) {
			LOGGER.info(new StringBuilder("\n")//
					.append("-------------------------------------------------------------------------\n")//
					.append("!! Fuseki server started. Listening on http://localhost:{}           !!\n")//
					.append("-------------------------------------------------------------------------")//
					.toString(), 3030);
			return true;
		}
		return false;
	}

	public String getPort() {
		String port = "3030";
		return port;
	}

	private boolean ping(String ip, int port) throws IOException {
		boolean response = false;
		Socket socket = new Socket();
		try {
			SocketAddress addr = new InetSocketAddress(ip, port);
			socket.connect(addr, 10);
			response = socket.isConnected();
		} catch (IOException e) {
			// LOGGER.warn("Unknown host {} Stack error :", ip,
			// e.getStackTrace());
			socket.close();
			return false;
		}
		socket.close();
		return response;
	}

	private void configTdbInstances(String[] predicates, String fuseki) {
		for (String predicate : predicates) {
			Path path = Paths.get( fuseki + "/run/configuration/" + predicate + ".ttl");
			if (Files.notExists(path)) {
				try {
					Files.createDirectories(path.getParent().toAbsolutePath());
				} catch (IOException e2) {
					LOGGER.warn("Couldn't create directory : {}", path.getParent().toAbsolutePath().normalize().toString());
				}
				try {
					Files.createFile(path);
				} catch (IOException e1) {
					LOGGER.warn("Couldn't create file : {}", path.toAbsolutePath().normalize().toString());
				}
				List<String> lines = new ArrayList<>();
				Path db = Paths.get( fuseki + "/run/databases/" + predicate + "/");
				lines.add("@prefix :      <http://base/#> .");
				lines.add("@prefix tdb:   <http://jena.hpl.hp.com/2008/tdb#> .");
				lines.add("@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .");
				lines.add("@prefix ja:    <http://jena.hpl.hp.com/2005/11/Assembler#> .");
				lines.add("@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .");
				lines.add("@prefix fuseki: <http://jena.apache.org/fuseki#> .");
				lines.add(":service_tdb_all  a                   fuseki:Service ;");
				lines.add("        rdfs:label                    \"TDB " + predicate + "\" ;");
				lines.add("        fuseki:dataset                :tdb_dataset_readwrite ;");
				lines.add("        fuseki:name                   \"" + predicate + "\" ;");
				lines.add("        fuseki:serviceQuery           \"query\" , \"sparql\" ;");
				lines.add("        fuseki:serviceReadGraphStore  \"get\" ;");
				lines.add("        fuseki:serviceReadWriteGraphStore");
				lines.add("               \"data\" ;");
				lines.add("        fuseki:serviceUpdate          \"update\" ;");
				lines.add("        fuseki:serviceUpload          \"upload\" .");
				lines.add("");
				lines.add(":tdb_dataset_readwrite");
				lines.add("        a             tdb:DatasetTDB ;");
				lines.add("        tdb:location  \"" + db.toAbsolutePath().normalize().toString().replaceAll("\\\\", "\\\\\\\\") + "\" .");
				try {
					Files.write(path, lines, Charset.forName("UTF-8"));
				} catch (IOException e) {
					LOGGER.warn("Couldn't write config file : {}", path.toAbsolutePath().normalize().toString());
				}
			}
		}
	}
}
