package com.sapik.webapp.domain.impl;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Normalizer;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.io.IOUtils;
import org.apache.jena.fuseki.Fuseki;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.ResultSetFormatter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.engine.http.QueryEngineHTTP;
import org.apache.jena.tdb.TDBFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sapik.webapp.config.AppSettings;
import com.sapik.webapp.domain.IFusekiConnector;
import com.sapik.webapp.utils.SparqlRequests;

@Component
public class FusekiConnector implements IFusekiConnector {

	private static final Logger LOGGER = LoggerFactory.getLogger(FusekiConnector.class);

//	private Map<String, DatasetAccessor> accessorMap;

	@Autowired
	private AppSettings settings;

	@Inject
	public FusekiConnector(AppSettings settings) {
//		accessorMap = new HashMap<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sapik.webapp.domain.IFusekiConnector#storeCsv(String path,
	 * String encoding)
	 */
	@Override
	public boolean storeCsv(String path, String encoding, String elementNamespace) {
		try {
			Path csvFile = Paths.get(path);
			String content;
			if (encoding.equals("ISO-8859-1")) {
				Charset charset = StandardCharsets.ISO_8859_1;
				content = new String(Files.readAllBytes(csvFile), charset);
				Normalizer.normalize(content, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
				content = path.contains("medecins_accredites") ? replaceSpecialCharactersForDoctors(content)
						: path.contains("opendata_es") ? replaceSpecialCharactersForFacilities(content)
								: path.contains("etalab") ? replaceSpecialCharactersForFacilities(content) : content;
			} else {
				Charset charset = StandardCharsets.UTF_8;
				// LOGGER.info("Selected read encoding : {}", charset.name());
				content = new String(Files.readAllBytes(csvFile), charset);
				content = replaceSpecialCharacters(content);
				content = content.replace(",\",", "\",").replace("\\s\"", "\"");
			}
			path = path.substring(0, path.lastIndexOf(File.separator) + 1).concat(getPredicate(elementNamespace))
					.concat(".csv");
			FileOutputStream output = new FileOutputStream(path);
			IOUtils.write(content, output, StandardCharsets.UTF_8);
			IOUtils.closeQuietly(output);
			Files.delete(csvFile);
			content = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		String predicate = getPredicate(elementNamespace);
		CsvToRdfReader parser = new CsvToRdfReader();
		Model m = parser.parseCsv(path, getHost(elementNamespace), predicate.substring(0, predicate.length() - 1),
				getPort(elementNamespace));
		parser = null;

		/**
		 * Here we will register data iin a TDB store
		 */
		String directory = Fuseki.DFT_FUSEKI_BASE + "/run/databases/doctors/";
		Dataset dataset = TDBFactory.createDataset(directory);
		try {
			dataset.begin(ReadWrite.WRITE);
			dataset.getDefaultModel().removeAll();
			dataset.getDefaultModel().add(m);
			dataset.commit();
		} finally {
			dataset.end();
			dataset=null;
			m = null;
		}
		LOGGER.debug("Got model from CSV file : {}", path);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sapik.webapp.domain.IFusekiConnector#storeCsv(String path,
	 * String encoding)
	 */
	@Override
	public boolean storeCsv(String path, String encoding, String elementNamespace, String categories,
			String colTitles) {
		try {
			Path csvFile = Paths.get(path);
			String content;
			if (encoding.equals("ISO-8859-1")) {
				Charset charset = StandardCharsets.ISO_8859_1;
				content = new String(Files.readAllBytes(csvFile), charset);
				Normalizer.normalize(content, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
				content = path.contains("medecins_accredites") ? replaceSpecialCharactersForDoctors(content)
						: path.contains("opendata_es") ? replaceSpecialCharactersForFacilities(content)
								: path.contains("etalab") ? replaceSpecialCharactersForFacilities(content) : content;
			} else {
				Charset charset = StandardCharsets.UTF_8;
				content = new String(Files.readAllBytes(csvFile), charset);
				content = replaceSpecialCharacters(content);
				content = content.replace(",\",", "\",").replace("\\s\"", "\"");
			}
			path = path.substring(0, path.lastIndexOf(File.separator) + 1).concat(getPredicate(elementNamespace))
					.concat(".csv");
			FileOutputStream output = new FileOutputStream(path);
			IOUtils.write(content, output, StandardCharsets.UTF_8);
			IOUtils.closeQuietly(output);
			Files.delete(csvFile);
			content = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		String predicate = getPredicate(elementNamespace);
		CsvToRdfReader parser = new CsvToRdfReader();
		Model m = parser.parseCsv(path, getHost(elementNamespace), predicate.substring(0, predicate.length() - 1),
				getPort(elementNamespace), categories, colTitles);
		parser = null;

		/**
		 * Here we will register data in a TDB store
		 */
		String directory = Fuseki.DFT_FUSEKI_BASE + "/run/databases/institutions/";
		Dataset dataset = TDBFactory.createDataset(directory);
		try {
			dataset.begin(ReadWrite.WRITE);
			dataset.getDefaultModel().removeAll();
			dataset.getDefaultModel().add(m);
			dataset.commit();
		} finally {
			dataset.end();
			dataset=null;
			m = null;
		}
		LOGGER.debug("Got model for CSV file : {}", path);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sapik.webapp.domain.IFusekiConnector#getIllnesses(String
	 * symptom)
	 */
	@Override
	public List<String> getIllnesses(String symptom) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sapik.webapp.domain.IFusekiConnector#getAllDoctors()
	 */
	@Override
	public String getAll(String url) {
//		Model medecins = accessorMap.get(getPredicate(url)).getModel();
//		StringWriter sb = new StringWriter();
//		RDFDataMgr.write(sb, medecins, Lang.RDFJSON);
//		return sb.toString();
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.sapik.webapp.domain.IFusekiConnector#getDoctor()
	 */
	@Override
	public String get(String url, String id) {
//		Model medecins = accessorMap.get(getPredicate(url)).getModel();
//		url = url.substring(url.length()) == "/" ? url.concat("/") : url;
//		Resource r = medecins.getResource(url.concat(id));
//		r.listProperties().toList();
		return "";
	}

	private String replaceSpecialCharactersForDoctors(String string) {
		string = string.replaceFirst("Nom du médecin", "nom").replaceFirst("Prénom du médecin", "prénom")
				.replaceFirst("Libellé long de la Spécialité du Médecin", "spécialité")
				.replaceFirst("Date d'accréditation du médecin", "accréditation")
				.replaceFirst("Nom de l'OA-A du Médecin", "OA-A").replaceFirst("Nom du département", "département")
				.replaceFirst("Région d'exercice", "région").replaceFirst("Code FINESS ", "finess")
				.replaceFirst("statut d'exercice", "statut");
		return replaceSpecialCharacters(string);
	}

	private String replaceSpecialCharactersForFacilities(String content) {
		content = content.replace('\'', '\"').replaceAll("\"\"", "\'").replace('É', 'E').replace('È', 'E')
				.replace('Ô', 'O').replace('Ï', 'I');
		return replaceSpecialCharacters(content);
	}

	private String replaceSpecialCharacters(String content) {
		content = content.replace(";;", ";null;").replace(';', ',').replaceAll(",\",", "\",").replaceAll("\\s\"", "\"");
		return content;
	}

	private String getHost(String url) {
		String hostname = url.substring(url.lastIndexOf("http://") + 7, url.lastIndexOf(":"));
		return hostname;
	}

	private String getPort(String url) {
		String port = url.substring(url.lastIndexOf(":") + 1, url.indexOf("/", url.lastIndexOf(":")));
		return port;
	}

	private String getPredicate(String url) {
		String[] l = url.split("/");
		String predicate = l[l.length - 1] == "" ? l[l.length - 2] : l[l.length - 1];
		return predicate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sapik.webapp.domain.IFusekiConnector#getInstitutionsInArea(java.lang.
	 * String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String getInstitutionsInArea(String xMin, String xMax, String yMin, String yMax) {
		String request = SparqlRequests.getMedicalFacilitiesInRangeRequest(xMin, xMax, yMin, yMax);
		LOGGER.debug("Querying institutions dataset ...");
		QueryEngineHTTP qexec = new QueryEngineHTTP("http://localhost:3030/institutions/sparql", request);
		ResultSet results = qexec.execSelect();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ResultSetFormatter.outputAsJSON(outputStream, results);
		while (results.hasNext()) {
			QuerySolution s = results.next();
			LOGGER.debug(s.toString());
		}
		qexec.close();
		return new String(outputStream.toByteArray());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.sapik.webapp.domain.IFusekiConnector#getInstitutionsInAreaForSpec(
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String getInstitutionsInAreaForSpec(String spec, String xMin, String xMax, String yMin, String yMax) {
		String request = "";
		LOGGER.info("Request for institutions in an area with spec : " + spec);
		switch (spec) {
		case "all":
			request = SparqlRequests.getMedicalFacilitiesInRangeRequest(xMin, xMax, yMin, yMax);
			break;
		case "feet":
			LOGGER.info("Got correspondance for request : " + settings.getRegionsCorrespondanceForFeet());
			request = SparqlRequests.getMedicalFacilitiesInRangeRequestForSpec(
					settings.getRegionsCorrespondanceForFeet(), xMin, xMax, yMin, yMax);
			break;
		case "head":
			LOGGER.info("Got correspondance for request : " + settings.getRegionsCorrespondanceForHead());
			request = SparqlRequests.getMedicalFacilitiesInRangeRequestForSpec(
					settings.getRegionsCorrespondanceForHead(), xMin, xMax, yMin, yMax);
			break;
		case "kidneys":
			LOGGER.info("Got correspondance for request : " + settings.getRegionsCorrespondanceForKidneys());
			request = SparqlRequests.getMedicalFacilitiesInRangeRequestForSpec(
					settings.getRegionsCorrespondanceForKidneys(), xMin, xMax, yMin, yMax);
			break;
		case "shins":
			LOGGER.info("Got correspondance for request : " + settings.getRegionsCorrespondanceForShins());
			request = SparqlRequests.getMedicalFacilitiesInRangeRequestForSpec(
					settings.getRegionsCorrespondanceForShins(), xMin, xMax, yMin, yMax);
			break;
		case "stomach":
			LOGGER.info("Got correspondance for request : " + settings.getRegionsCorrespondanceForStomach());
			request = SparqlRequests.getMedicalFacilitiesInRangeRequestForSpec(
					settings.getRegionsCorrespondanceForStomach(), xMin, xMax, yMin, yMax);
			break;
		case "shoulders":
			LOGGER.info("Got correspondance for request : " + settings.getRegionsCorrespondanceForShoulders());
			request = SparqlRequests.getMedicalFacilitiesInRangeRequestForSpec(
					settings.getRegionsCorrespondanceForShoulders(), xMin, xMax, yMin, yMax);
			break;
		case "lungs":
			LOGGER.info("Got correspondance for request : " + settings.getRegionsCorrespondanceForLungs());
			request = SparqlRequests.getMedicalFacilitiesInRangeRequestForSpec(
					settings.getRegionsCorrespondanceForLungs(), xMin, xMax, yMin, yMax);
			break;
		case "knees":
			LOGGER.info("Got correspondance for request : " + settings.getRegionsCorrespondanceForKnees());
			request = SparqlRequests.getMedicalFacilitiesInRangeRequestForSpec(
					settings.getRegionsCorrespondanceForKnees(), xMin, xMax, yMin, yMax);
			break;
		case "womankidneys":
			LOGGER.info("Got correspondance for request : " + settings.getRegionsCorrespondanceForKidneysWoman());
			request = SparqlRequests.getMedicalFacilitiesInRangeRequestForSpec(
					settings.getRegionsCorrespondanceForKidneysWoman(), xMin, xMax, yMin, yMax);
			break;
		default:
			break;
		}
		LOGGER.debug("Querying institutions dataset ...");
		LOGGER.debug(request);
		QueryEngineHTTP qexec = new QueryEngineHTTP("http://localhost:3030/institutions/sparql", request);
		ResultSet results = qexec.execSelect();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		ResultSetFormatter.outputAsJSON(outputStream, results);
//		while (results.hasNext()) {
//			QuerySolution s = results.next();
//			LOGGER.debug(s.toString());
//		}
		qexec.close();
		return new String(outputStream.toByteArray());
	}

}
