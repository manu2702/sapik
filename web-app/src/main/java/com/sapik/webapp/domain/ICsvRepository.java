package com.sapik.webapp.domain;

public interface ICsvRepository {

	public Thread getCsvFile(final String fileUrl, final String namespace);
	
	public Thread getCsvFile(final String fileUrl, final String namespace, final String categories, final String colTitles);

}
