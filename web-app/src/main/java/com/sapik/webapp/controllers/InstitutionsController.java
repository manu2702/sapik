package com.sapik.webapp.controllers;

import java.io.IOException;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.codahale.metrics.annotation.Timed;
import com.sapik.webapp.controllers.BaseController;
import com.sapik.webapp.services.SapikWebappService;

/**
 * The Class TripPlannerController
 */
@Controller
@RequestMapping(value = BaseController.BASE_PATH)
public class InstitutionsController extends BaseController {

	/** The LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(InstitutionsController.class);
	
	/** Service URI to reload file */
	private static final String URI_ALL_INSTITUTIONS = "/institutions/all";
	/** Reload file request URI */
	public static final String URI_ALL_INSTITUTIONS_ABS = BASE_PATH + URI_ALL_INSTITUTIONS;
	
	/** Service URI to reload file */
	private static final String URI_INSTITUTION = "/institutions/{institutionId}";
	/** Reload file request URI */
	public static final String URI_INSTITUTION_ABS = BASE_PATH + URI_INSTITUTION;

	/** Service URI to reload file */
	private static final String URI_INSTITUTION_GEO = "/institutions/geoloc";
	/** Reload file request URI */
	public static final String URI_INSTITUTION_GEO_ABS = BASE_PATH + URI_INSTITUTION_GEO;

	/** Service URI to reload file */
	private static final String URI_INSTITUTION_GEO_SPEC = "/institutions/geolocalisation/{spec}";
	/** Reload file request URI */
	public static final String URI_INSTITUTION_GEO_SPEC_ABS = BASE_PATH + URI_INSTITUTION_GEO;

	private SapikWebappService service;

	/**
	 * Constructor
	 */
	@Inject
	public InstitutionsController(SapikWebappService service) {
		this.service = service;
	}


	/**
	 * Get the list of all medical facilities in France
	 * 
	 * @param file
	 * @return no content
	 */
	@Timed
	@RequestMapping(value = URI_ALL_INSTITUTIONS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllInstitutions() throws IOException {
		LOGGER.debug("REST request to get all institutions");
		String result = service.getAllInstitutions();
		return ResponseEntity.ok(result);
	}
	
	/**
	 * Get a medical facility informations
	 * 
	 * @param file
	 * @return no content
	 */
	@Timed
	@RequestMapping(value = URI_INSTITUTION, method = RequestMethod.GET)
	public ResponseEntity<?> getInstitution(@PathVariable("institutionId") String institutionId) throws IOException {
		LOGGER.debug("REST request to get a particular facility");
		String result = service.getFacility(institutionId);
		return ResponseEntity.ok().body(result);
	}

	/**
	 * Gets institutions in a coordinates-delimited area
	 * 
	 * @param xMin
	 * @param xMax
	 * @param yMin
	 * @param yMax
	 * @return the list of institutions
	 * @throws IOException
	 */
	@Timed
	@RequestMapping(value = URI_INSTITUTION_GEO, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getInstitutionsInArea(@RequestParam("xMin") String xMin,
												 @RequestParam("xMax") String xMax,
												 @RequestParam("yMin") String yMin,
												 @RequestParam("yMax") String yMax) throws IOException {
		LOGGER.debug("REST request to get institutions in a given area delimited by coordinates");
		String result = service.getInstitutionsInArea(xMin, xMax, yMin, yMax);
		return ResponseEntity.ok().body(result);
	}

	/**
	 * Gets institutions in a coordinates-delimited area
	 * 
	 * @param xMin
	 * @param xMax
	 * @param yMin
	 * @param yMax
	 * @return the list of institutions
	 * @throws IOException
	 */
	@Timed
	@RequestMapping(value = URI_INSTITUTION_GEO_SPEC, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getInstitutionsInAreaForSpec(@PathVariable("spec") String spec,
												 @RequestParam("xMin") String xMin,
												 @RequestParam("xMax") String xMax,
												 @RequestParam("yMin") String yMin,
												 @RequestParam("yMax") String yMax) throws IOException {
		LOGGER.debug("REST request to get institutions in a given area delimited by coordinates with spec");
		String result = service.getInstitutionsInAreaForSpec(spec, xMin, xMax, yMin, yMax);
		return ResponseEntity.ok().body(result);
	}
	/**
	 * ========================================================================
	 * = PRIVATE : methods
	 * ========================================================================
	 */
}

