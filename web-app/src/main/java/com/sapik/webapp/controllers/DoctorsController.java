package com.sapik.webapp.controllers;

import java.io.IOException;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.codahale.metrics.annotation.Timed;
import com.sapik.webapp.controllers.BaseController;
import com.sapik.webapp.services.SapikWebappService;

/**
 * The Class TripPlannerController
 */
@Controller
@RequestMapping(value = BaseController.BASE_PATH)
public class DoctorsController extends BaseController {

	/** The LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DoctorsController.class);

	/** Service URI to reload file */
	private static final String URI_FOR_SIMPTOMS = "/symptoms/illnessFor";
	/** Reload file request URI */
	public static final String URI_FOR_SIMPTOMS_ABS = BASE_PATH + URI_FOR_SIMPTOMS;
	
	/** Service URI to reload file */
	private static final String URI_ALL_DOCTORS = "/doctors/all";
	/** Reload file request URI */
	public static final String URI_ALL_DOCTORS_ABS = BASE_PATH + URI_ALL_DOCTORS;
	
	/** Service URI to reload file */
	private static final String URI_DOCTOR = "/doctors/{doctorId}";
	/** Reload file request URI */
	public static final String URI_DOCTOR_ABS = BASE_PATH + URI_DOCTOR;

	private SapikWebappService service;

	/**
	 * Constructor
	 */
	@Inject
	public DoctorsController(SapikWebappService service) {
		this.service = service;
	}

	/**
	 * Uploads the YAML file data
	 * 
	 * @param file
	 * @return no content
	 */
	@Timed
	@RequestMapping(value = URI_FOR_SIMPTOMS, method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = "application/json")
	public ResponseEntity<?> symptomsIllnessFor(@RequestParam final String symptom) throws IOException {
		LOGGER.debug("REST request to upload CSV file at : '{}'", symptom);
		return ResponseEntity.ok().build();
	}

	/**
	 * Uploads the YAML file data
	 * 
	 * @param file
	 * @return no content
	 */
	@Timed
	@RequestMapping(value = URI_ALL_DOCTORS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getAllDoctors() throws IOException {
		LOGGER.debug("REST request to get all doctors");
		String result = service.getAllDoctors();
		return ResponseEntity.ok(result);
	}
	
	/**
	 * Uploads the YAML file data
	 * 
	 * @param file
	 * @return no content
	 */
	@Timed
	@RequestMapping(value = URI_DOCTOR, method = RequestMethod.GET)
	public ResponseEntity<?> getDoctor(@PathVariable("doctorId") String doctorId) throws IOException {
		LOGGER.debug("REST request to get a particular doctor");
		String result = service.getDoctor(doctorId);
		return ResponseEntity.ok().body(result);
	}
	/**
	 * ========================================================================
	 * = PRIVATE : methods
	 * ========================================================================
	 */
}
