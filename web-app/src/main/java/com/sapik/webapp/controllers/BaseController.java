package com.sapik.webapp.controllers;

import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

/**
 * <p>
 * Superclass for all REST services
 * </p>
 */
public abstract class BaseController {
    public static final String BASE_PATH = "/api";

    /**
     * Prepare a builder from the host, port, scheme, and context path of the
     * current request
     * 
     * @param absolutePath
     *            The URI path
     * @return The URI components
     */
    protected UriComponents uribuilder(String absolutePath) {
        return ServletUriComponentsBuilder.fromCurrentContextPath().path(absolutePath).build();
    }

    /**
     * Prepare a builder from the host, port, scheme, and path of the current
     * request
     * 
     * @return The URI string
     */
    protected String currentRequestAbsoluteUri() {
        return ServletUriComponentsBuilder.fromCurrentRequestUri().build().toUriString();
    }
}
