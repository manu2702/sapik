package com.sapik.webapp.controllers;

import java.io.File;
import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.codahale.metrics.annotation.Timed;
import com.sapik.webapp.controllers.BaseController;
import com.sapik.webapp.services.SapikWebappService;

/**
 * The Class TripPlannerController
 */
@Controller
@RequestMapping(value = BaseController.BASE_PATH)
public class SapikWebappController extends BaseController {

	/** The LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(SapikWebappController.class);

	@Autowired
	private HttpServletRequest request;

	/** Service URI to reload file */
	private static final String URI_UPL_UTF_8 = "/upload-UTF-8";
	/** Reload file request URI */
	public static final String URI_UPL_UTF_8_ABS = BASE_PATH + URI_UPL_UTF_8;

	/** Service URI to reload file */
	private static final String URI_UPL_ISO_8859_1 = "/upload-ISO-8859-1";
	/** Reload file request URI */
	public static final String URI_UPL_ISO_8859_1_ABS = BASE_PATH + URI_UPL_ISO_8859_1;

	/** Service URI to reload file */
	private static final String URI_FOR_SIMPTOMS = "/symptoms/illnessFor";
	/** Reload file request URI */
	public static final String URI_FOR_SIMPTOMS_ABS = BASE_PATH + URI_FOR_SIMPTOMS;

	private SapikWebappService service;

	/**
	 * Constructor
	 */
	@Inject
	public SapikWebappController(SapikWebappService service) {
		this.service = service;
	}

	/**
	 * Uploads the doctors data with UTF-8 encoded CSV
	 * 
	 * @param file
	 * @return 200 | 202
	 */
	@Timed
	@RequestMapping(value = URI_UPL_UTF_8, method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> uploadFileUTF(@RequestBody(required = false) MultipartFile file,
										   @RequestParam(required = false) final String url,
										   @RequestParam(required = false) final String namespace) throws IOException {
		if(file == null && url == null && namespace == null) {
			return ResponseEntity.noContent().build();
		}

		LOGGER.debug("REST request to upload CSV file : '{}'", file);

		String uploadsDir = "/uploads/";
		String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
		if (!new File(realPathtoUploads).exists())
			new File(realPathtoUploads).mkdir();

		String originalName = file.getOriginalFilename();
		final String filePath = realPathtoUploads + originalName;
		File dest = new File(filePath);
		file.transferTo(dest);
		service.saveFile(filePath, "UTF-8", url, namespace);
		return ResponseEntity.ok().build();
	}

	/**
	 * Uploads the doctors data with ISO-8859-1 encoded CSV
	 * 
	 * @param file
	 * @return 200 | 202
	 */
	@Timed
	@RequestMapping(value = URI_UPL_ISO_8859_1, method = RequestMethod.POST)
	public ResponseEntity<?> uploadFileISO(@RequestBody(required = false) MultipartFile file,
			   @RequestParam(required = false) final String url,
			   @RequestParam(required = false) final String namespace) throws IOException {
		if(file == null && url == null && namespace == null) {
			return ResponseEntity.noContent().build();
		}

		LOGGER.debug("REST request to upload CSV file : '{}'", file);
		String uploadsDir = "/uploads/";
		String realPathtoUploads = request.getServletContext().getRealPath(uploadsDir);
		if (!new File(realPathtoUploads).exists())
			new File(realPathtoUploads).mkdir();

		String originalName = file.getOriginalFilename();
		final String filePath = realPathtoUploads + originalName;
		File dest = new File(filePath);
		file.transferTo(dest);
		service.saveFile(filePath, "ISO-8859-1", url, namespace);
		return ResponseEntity.ok().build();
	}

	/**
	 * ========================================================================
	 * = PRIVATE : methods
	 * ========================================================================
	 */
}
