/**
 * 
 */
package com.sapik.webapp.utils;

import java.util.List;

/**
 * @author eazmed
 *
 */
public final class SparqlRequests {

	private SparqlRequests() {
	}

	public static String getMedicalFacilitiesInRangeRequest(String xMin, String xMax, String yMin, String yMax) {
		return " SELECT ?finess ?coordX ?coordY ?nom ?nomLong ?numVoie ?typVoie ?voie ?compVoie ?lieuDitBp ?commune ?departement ?libDepartement ?ligneAcheminement ?tel ?fax\n"
				+ " WHERE {\n"
					+ " ?subject <http://localhost:3030/institutions#institution/geolocalisation/coordxet> ?coordX\n"
					+ " FILTER( ?coordX > \"" + xMin + "\" && ?coordX < \"" + xMax + "\" )\n"
					+ " {\n"
						+ " SELECT ?subject ?coordY WHERE {\n"
						+ " ?subject <http://localhost:3030/institutions#institution/geolocalisation/coordyet> ?coordY\n"
						+ " FILTER( ?coordY > \"" + yMin + "\" && ?coordY < \"" + yMax + "\" ) } \n"
					+ " } \n"
				+ " . ?subject <http://localhost:3030/institutions#institution/geolocalisation/gnofinesset> ?finess\n"
				+ " . ?subjectStruct <http://localhost:3030/institutions#institution/structureet/snofinesset> ?finess\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/rs> ?nom}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/rslongue> ?nomLong}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/numvoie> ?numVoie}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/typvoie> ?typVoie}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/voie> ?voie}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/compvoie> ?compVoie}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/lieuditbp> ?lieuDitBp}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/commune> ?commune}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/departement> ?departement}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/libdepartement> ?libDepartement}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/ligneacheminement> ?ligneAcheminement}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/telephone> ?tel}\n"
				+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/telecopie> ?fax}\n"
				+ "}";
	}

	public static String getMedicalFacilitiesInRangeRequestForSpec(List<String> spec, String xMin, String xMax, String yMin, String yMax) {
		String add = "";
		int i=0;
		if(spec.size()>1) {
			while (i < spec.size()-1) {
				add = add.concat(spec.get(i)).concat("\", \"i\" )}\n"
				+ "	UNION { ?subjectDoctor <http://localhost:3030/doctors#doctor/spécialité> ?objectD\n"
				+ "    . FILTER regex(str(?objectD), \"");
				i++;
			}
		}
		add = add.concat(spec.get(i));
		return " SELECT ?finess ?coordX ?coordY ?nom ?nomLong ?numVoie ?typVoie ?voie ?compVoie ?lieuDitBp ?commune ?departement ?libDepartement ?ligneAcheminement ?tel ?fax ?objectD\n"
				+ " WHERE {\n"
				+ " SERVICE <http://localhost:3030/doctors> {\n"
				+ " {\n"
				+ " ?subjectDoctor <http://localhost:3030/doctors#doctor/spécialité> ?objectD\n"
				+ " FILTER regex(str(?objectD), \"" + add.trim() + "\", \"i\" )}\n"
				+ " . ?subjectDoctor <http://localhost:3030/doctors#doctor/finess> ?finess\n"
				+ " }\n"
				+ " . ?subject <http://localhost:3030/institutions#institution/geolocalisation/gnofinesset> ?finess\n"
				+ " . ?subject <http://localhost:3030/institutions#institution/geolocalisation/coordxet> ?coordX\n"
				+ " FILTER( ?coordX > \"" + xMin + "\" && ?coordX < \"" + xMax + "\" )\n"
				+ " {\n"
					+ " SELECT ?subject ?coordY WHERE {\n"
					+ " ?subject <http://localhost:3030/institutions#institution/geolocalisation/coordyet> ?coordY\n"
					+ " FILTER( ?coordY > \"" + yMin + "\" && ?coordY < \"" + yMax + "\" ) }\n"
				+ " }\n"
			+ " . ?subjectStruct <http://localhost:3030/institutions#institution/structureet/snofinesset> ?finess\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/rs> ?nom}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/rslongue> ?nomLong}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/numvoie> ?numVoie}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/typvoie> ?typVoie}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/voie> ?voie}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/compvoie> ?compVoie}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/lieuditbp> ?lieuDitBp}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/commune> ?commune}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/departement> ?departement}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/libdepartement> ?libDepartement}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/ligneacheminement> ?ligneAcheminement}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/telephone> ?tel}\n"
			+ " OPTIONAL{?subjectStruct <http://localhost:3030/institutions#institution/structureet/telecopie> ?fax}\n"
			+ "}";
	}
}
