package com.sapik.webapp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>
 * Configuration properties for Swagger
 * </p>
 */

@ConfigurationProperties(SwaggerSettings.PREFIX)
class SwaggerSettings {
    public static final String PREFIX = "sapik.swagger";

    private String version = "1.0";
    private String title;
    private String description;
    private String termsOfServiceUrl;
    private String contact;
    private String license;
    private String licenseUrl;
    private boolean enabled = true;

    public String getVersion() {
        return this.version;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getTermsOfServiceUrl() {
        return termsOfServiceUrl;
    }

    public String getContact() {
        return contact;
    }

    public String getLicense() {
        return license;
    }

    public String getLicenseUrl() {
        return licenseUrl;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTermsOfServiceUrl(String termsOfServiceUrl) {
        this.termsOfServiceUrl = termsOfServiceUrl;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public void setLicenseUrl(String licenseUrl) {
        this.licenseUrl = licenseUrl;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
