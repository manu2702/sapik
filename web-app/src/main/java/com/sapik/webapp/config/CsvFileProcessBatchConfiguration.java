package com.sapik.webapp.config;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.sapik.webapp.batch.CsvFileProcessBatch;
import com.sapik.webapp.domain.IFusekiConnector;

@Configuration
@Import(BatchInfrastructureConfiguration.class)
public class CsvFileProcessBatchConfiguration {

	private static final Logger LOGGER = LoggerFactory.getLogger(CsvFileProcessBatchConfiguration.class);

	@Inject
	private JobBuilderFactory jobBuilders;

	@Inject
	private StepBuilderFactory stepBuilders;

	@Inject
	private IFusekiConnector fuseki;
//
//	@Inject
//	private SimpleAsyncTaskExecutor taskExecutor;

	@Bean
	public Job csvFileProcessJob(){
		LOGGER.info("Setting csvFileProcessJob bean");
	    return jobBuilders.get("csvFileProcessJob")
	            .start(step())
	            .build();
	}
	@Bean
	public Step step(){
	    return stepBuilders.get("csvFileProcessStep")
	            .tasklet(csvFileProcessBatch())
	            .build();
	}
	@Bean
	public CsvFileProcessBatch csvFileProcessBatch() {
	    return new CsvFileProcessBatch(fuseki);
	}
}
