package com.sapik.webapp.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>
 * Configuration properties for this service
 * </p>
 */
@ConfigurationProperties(prefix = AppSettings.PREFIX)
public class AppSettings {
    public static final String PREFIX = "sapik.webapp";

    private String dataPath;
    private List<String> csvPredicates;
    private List<String> csvUrls;
    private List<String> csvNamespaces;
    private String csvEtalabCategories;
    private String etalabDataTitles;
    private List<String> regionsCorrespondanceForHead;
    private List<String> regionsCorrespondanceForKidneys;
    private List<String> regionsCorrespondanceForKnees;
    private List<String> regionsCorrespondanceForLungs;
    private List<String> regionsCorrespondanceForShins;
    private List<String> regionsCorrespondanceForStomach;
    private List<String> regionsCorrespondanceForFeet;
    private List<String> regionsCorrespondanceForShoulders;
    private List<String> regionsCorrespondanceForOthers;
    private List<String> regionsCorrespondanceForKidneysWoman;

    public String getDataPath() {
        return dataPath;
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }
    

	/**
	 * @return the csvPredicates
	 */
	public List<String> getCsvPredicates() {
		return csvPredicates;
	}

	/**
	 * @param csvPredicates the csvPredicates to set
	 */
	public void setCsvPredicates(List<String> csvPredicates) {
		this.csvPredicates = csvPredicates;
	}

	/**
	 * @return the csvUrls
	 */
	public List<String> getCsvUrls() {
		return csvUrls;
	}

	/**
	 * @param csvUrls the csvUrls to set
	 */
	public void setCsvUrls(List<String> csvUrls) {
		this.csvUrls = csvUrls;
	}

	/**
	 * @return the csvNamespaces
	 */
	public List<String> getCsvNamespaces() {
		return csvNamespaces;
	}

	/**
	 * @param csvNamespaces the csvNamespaces to set
	 */
	public void setCsvNamespaces(List<String> csvNamespaces) {
		this.csvNamespaces = csvNamespaces;
	}

	/**
	 * 
	 * @return the etalab's file categories
	 */
	public String getCsvEtalabCategories() {
		return csvEtalabCategories;
	}

	/**
	 * 
	 * @param csvEtalabCategories
	 */
	public void setCsvEtalabCategories(String csvEtalabCategories) {
		this.csvEtalabCategories = csvEtalabCategories;
	}

	/**
	 * 
	 * @return the etalab's file colon titles
	 */
	public String getEtalabDataTitles() {
		return etalabDataTitles;
	}

	/**
	 * 
	 * @param etalabDataTitles
	 */
	public void setEtalabDataTitles(String etalabDataTitles) {
		this.etalabDataTitles = etalabDataTitles;
	}

	/**
	 * @return the regionsCorrespondanceForHead
	 */
	public List<String> getRegionsCorrespondanceForHead() {
		return regionsCorrespondanceForHead;
	}

	/**
	 * @param regionsCorrespondanceForHead the regionsCorrespondanceForHead to set
	 */
	public void setRegionsCorrespondanceForHead(List<String> regionsCorrespondanceForHead) {
		this.regionsCorrespondanceForHead = regionsCorrespondanceForHead;
	}

	/**
	 * @return the regionsCorrespondanceForKidneys
	 */
	public List<String> getRegionsCorrespondanceForKidneys() {
		return regionsCorrespondanceForKidneys;
	}

	/**
	 * @param regionsCorrespondanceForKidneys the regionsCorrespondanceForKidneys to set
	 */
	public void setRegionsCorrespondanceForKidneys(List<String> regionsCorrespondanceForKidneys) {
		this.regionsCorrespondanceForKidneys = regionsCorrespondanceForKidneys;
	}

	/**
	 * @return the regionsCorrespondanceForKnees
	 */
	public List<String> getRegionsCorrespondanceForKnees() {
		return regionsCorrespondanceForKnees;
	}

	/**
	 * @param regionsCorrespondanceForKnees the regionsCorrespondanceForKnees to set
	 */
	public void setRegionsCorrespondanceForKnees(List<String> regionsCorrespondanceForKnees) {
		this.regionsCorrespondanceForKnees = regionsCorrespondanceForKnees;
	}

	/**
	 * @return the regionsCorrespondanceForLungs
	 */
	public List<String> getRegionsCorrespondanceForLungs() {
		return regionsCorrespondanceForLungs;
	}

	/**
	 * @param regionsCorrespondanceForLungs the regionsCorrespondanceForLungs to set
	 */
	public void setRegionsCorrespondanceForLungs(List<String> regionsCorrespondanceForLungs) {
		this.regionsCorrespondanceForLungs = regionsCorrespondanceForLungs;
	}

	/**
	 * @return the regionsCorrespondanceForShins
	 */
	public List<String> getRegionsCorrespondanceForShins() {
		return regionsCorrespondanceForShins;
	}

	/**
	 * @param regionsCorrespondanceForShins the regionsCorrespondanceForShins to set
	 */
	public void setRegionsCorrespondanceForShins(List<String> regionsCorrespondanceForShins) {
		this.regionsCorrespondanceForShins = regionsCorrespondanceForShins;
	}

	/**
	 * @return the regionsCorrespondanceForStomach
	 */
	public List<String> getRegionsCorrespondanceForStomach() {
		return regionsCorrespondanceForStomach;
	}

	/**
	 * @param regionsCorrespondanceForStomach the regionsCorrespondanceForStomach to set
	 */
	public void setRegionsCorrespondanceForStomach(List<String> regionsCorrespondanceForStomach) {
		this.regionsCorrespondanceForStomach = regionsCorrespondanceForStomach;
	}

	/**
	 * @return the regionsCorrespondanceForFeet
	 */
	public List<String> getRegionsCorrespondanceForFeet() {
		return regionsCorrespondanceForFeet;
	}

	/**
	 * @param regionsCorrespondanceForFeet the regionsCorrespondanceForFeet to set
	 */
	public void setRegionsCorrespondanceForFeet(List<String> regionsCorrespondanceForFeet) {
		this.regionsCorrespondanceForFeet = regionsCorrespondanceForFeet;
	}

	/**
	 * @return the regionsCorrespondanceForShoulders
	 */
	public List<String> getRegionsCorrespondanceForShoulders() {
		return regionsCorrespondanceForShoulders;
	}

	/**
	 * @param regionsCorrespondanceForShoulders the regionsCorrespondanceForShoulders to set
	 */
	public void setRegionsCorrespondanceForShoulders(List<String> regionsCorrespondanceForShoulders) {
		this.regionsCorrespondanceForShoulders = regionsCorrespondanceForShoulders;
	}

	/**
	 * @return the regionsCorrespondanceForOthers
	 */
	public List<String> getRegionsCorrespondanceForOthers() {
		return regionsCorrespondanceForOthers;
	}

	/**
	 * @param regionsCorrespondanceForOthers the regionsCorrespondanceForOthers to set
	 */
	public void setRegionsCorrespondanceForOthers(List<String> regionsCorrespondanceForOthers) {
		this.regionsCorrespondanceForOthers = regionsCorrespondanceForOthers;
	}

	/**
	 * @return the regionsCorrespondanceForKidneysWoman
	 */
	public List<String> getRegionsCorrespondanceForKidneysWoman() {
		return regionsCorrespondanceForKidneysWoman;
	}

	/**
	 * @param regionsCorrespondanceForKidneysWoman the regionsCorrespondanceForKidneysWoman to set
	 */
	public void setRegionsCorrespondanceForKidneysWoman(List<String> regionsCorrespondanceForKidneysWoman) {
		this.regionsCorrespondanceForKidneysWoman = regionsCorrespondanceForKidneysWoman;
	}

}
