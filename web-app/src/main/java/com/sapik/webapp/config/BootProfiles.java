package com.sapik.webapp.config;

/**
 * <p>
 * List of the Spring Boot profiles
 * </p>
 * 
 */
public enum BootProfiles {
    DEV("dev"), PROD("prod");

    @Override
    public String toString() {
        return this.name;
    }

    private BootProfiles(String name) {
        this.name = name;
    }

    private String name;
}
