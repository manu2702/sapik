package com.sapik.webapp.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.explore.support.MapJobExplorerFactoryBean;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

@Configuration
@EnableBatchProcessing
public class BatchInfrastructureConfiguration {

	private static final Logger LOGGER = LoggerFactory.getLogger(BatchInfrastructureConfiguration.class);

	private MapJobRepositoryFactoryBean repoFactory;

	@Bean
	public JobLauncher getJobLauncher() throws Exception {
		LOGGER.info("Setting Job launcher");
		SimpleJobLauncher jobLauncher = new SimpleJobLauncher();
		jobLauncher.setJobRepository(getJobRepository());
		jobLauncher.setTaskExecutor(getTaskExecutor());
		jobLauncher.afterPropertiesSet();
		return jobLauncher;
	}

	@Bean
	public JobRepository getJobRepository() throws Exception {
		if (this.repoFactory == null) {
			this.repoFactory = new MapJobRepositoryFactoryBean();
			this.repoFactory.setTransactionManager(new ResourcelessTransactionManager());
			this.repoFactory.afterPropertiesSet();
		}
		return (JobRepository) this.repoFactory.getObject();
	}

	@Bean
	public JobExplorer getJobExplorer() throws Exception {
		if (this.repoFactory == null) {
			this.repoFactory = new MapJobRepositoryFactoryBean();
			this.repoFactory.setTransactionManager(new ResourcelessTransactionManager());
			this.repoFactory.afterPropertiesSet();
		}
		MapJobExplorerFactoryBean jobExplorerFactory = new MapJobExplorerFactoryBean(this.repoFactory);
		jobExplorerFactory.afterPropertiesSet();
		return jobExplorerFactory.getObject();
	}

	// @Bean
	// public JobOperator getJobOperator() throws Exception {
	// SimpleJobOperator jobOperator = new SimpleJobOperator();
	// jobOperator.setJobLauncher(getJobLauncher());
	// jobOperator.setJobRepository(getJobRepository());
	// jobOperator.afterPropertiesSet();
	// return jobOperator;
	// }
	@Bean
	public JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor(JobRegistry jobRegistry) {
		JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor = new JobRegistryBeanPostProcessor();
		jobRegistryBeanPostProcessor.setJobRegistry(jobRegistry);
		return jobRegistryBeanPostProcessor;
	}

	@Bean
	public SimpleAsyncTaskExecutor getTaskExecutor() {
		SimpleAsyncTaskExecutor taskExecutor = new SimpleAsyncTaskExecutor();
		taskExecutor.setConcurrencyLimit(10);
		return taskExecutor;
	}
}
