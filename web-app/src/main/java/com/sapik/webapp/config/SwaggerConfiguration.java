package com.sapik.webapp.config;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableConfigurationProperties(SwaggerSettings.class)
@ConditionalOnProperty(matchIfMissing = true, name = SwaggerSettings.PREFIX + ".enabled")
@EnableSwagger2
public class SwaggerConfiguration {
    private static final String DEFAULT_INCLUDE_PATTERN = "/api/.*";
    private static final Logger log = LoggerFactory.getLogger(SwaggerConfiguration.class);

    @Autowired
    private SwaggerSettings config;

    /**
     * Swagger Spring MVC configuration.
     * 
     * @return docket
     */
    @Bean
    public Docket swaggerSpringfoxDocket() {
        Docket docket = null;

        log.debug("Starting Swagger");
        if (config.isEnabled()) {
            StopWatch watch = new StopWatch();
            watch.start();

            // @formatter:off
            docket = new Docket(DocumentationType.SWAGGER_2)
                    .apiInfo(apiInfo())
                    .genericModelSubstitutes(ResponseEntity.class)
                    .forCodeGeneration(true)
                    .genericModelSubstitutes(ResponseEntity.class)
                    .directModelSubstitute(org.joda.time.LocalDate.class, String.class)
                    .directModelSubstitute(org.joda.time.LocalDateTime.class, Date.class)
                    .directModelSubstitute(org.joda.time.DateTime.class, Date.class)
                    .select()
                    .apis(RequestHandlerSelectors.any())
                    .paths(regex(DEFAULT_INCLUDE_PATTERN))
                    .build();
            // @formatter:on
            watch.stop();
            log.debug("Started Swagger in {} ms", watch.getTotalTimeMillis());
        } else {
            log.debug("Swagger is not enabled.");
        }
        return docket;
    }

    /**
     * API Info as it appears on the swagger-ui page.
     */
    private ApiInfo apiInfo() {
//    	Contact c = new Contact();
//    	c.setEmail(config.getContact());
        // @formatter:off
    	return new ApiInfoBuilder()
                .title(config.getTitle())
                .description(config.getDescription())
                .version(config.getVersion())
                .termsOfServiceUrl(config.getTermsOfServiceUrl())
                .license(config.getLicense())
                .licenseUrl(config.getLicenseUrl())
                .build();
//        return new ApiInfoBuilder(
//                config.getTitle(),
//                config.getDescription(),
//                config.getVersion(),
//                config.getTermsOfServiceUrl(),
//                new Contact(),
//                config.getLicense(),
//                config.getLicenseUrl());
        // @formatter:on
    }
}
