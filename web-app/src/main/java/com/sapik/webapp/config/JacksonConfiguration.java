package com.sapik.webapp.config;

import org.joda.time.DateTime;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.datetime.joda.DateTimeFormatterFactory;

import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.joda.cfg.JacksonJodaDateFormat;
import com.fasterxml.jackson.datatype.joda.ser.DateTimeSerializer;

/**
 * <p>
 * Configure Jackson (de)serializer
 * </p>
 * 
 */

@Configuration
public class JacksonConfiguration {

    @Bean
    public JodaModule jacksonJodaModule() {
        DateTimeFormatterFactory formatterFactory = new DateTimeFormatterFactory();
        formatterFactory.setIso(DateTimeFormat.ISO.DATE_TIME);

        JodaModule module = new JodaModule();
        module.addSerializer(//
                DateTime.class, //
                new DateTimeSerializer(//
                        new JacksonJodaDateFormat(//
                                formatterFactory//
                                        .createDateTimeFormatter()//
                                        .withZoneUTC())));
        return module;
    }
}
