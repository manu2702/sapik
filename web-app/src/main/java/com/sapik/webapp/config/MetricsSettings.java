package com.sapik.webapp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * <p>
 * Configuration properties for Metrics
 * </p>
 */

@ConfigurationProperties(MetricsSettings.PREFIX)
class MetricsSettings {
    public static final String PREFIX = "sapik.metrics";

    private boolean enabled = true;

    public boolean isEnabled() {
        return this.enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
