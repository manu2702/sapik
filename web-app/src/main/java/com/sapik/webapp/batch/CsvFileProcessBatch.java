package com.sapik.webapp.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.sapik.webapp.domain.IFusekiConnector;

@Component
public class CsvFileProcessBatch implements Tasklet {

	private IFusekiConnector fuseki;

	private static final Logger LOGGER = LoggerFactory.getLogger(CsvFileProcessBatch.class);

	public CsvFileProcessBatch(IFusekiConnector fuseki) {
		this.fuseki = fuseki;
	}

	@Async
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) {
		String csvFilePath = chunkContext.getStepContext()
								  .getStepExecution()
								  .getJobExecution()
								  .getJobParameters()
								  .getString("csvFilePath");
		String encoding = chunkContext.getStepContext()
				  .getStepExecution()
				  .getJobExecution()
				  .getJobParameters()
				  .getString("encoding");
		String namespace = chunkContext.getStepContext()
				  .getStepExecution()
				  .getJobExecution()
				  .getJobParameters()
				  .getString("namespace");
		String categories = chunkContext.getStepContext()
				  .getStepExecution()
				  .getJobExecution()
				  .getJobParameters()
				  .getString("categories");
		String colTitles = chunkContext.getStepContext()
				  .getStepExecution()
				  .getJobExecution()
				  .getJobParameters()
				  .getString("colTitles");
		boolean success;
		if(categories != null && colTitles != null) {
//			LOGGER.debug("Storing CSV institutions file from path : {}", csvFilePath);
			success = fuseki.storeCsv(csvFilePath, encoding, namespace, categories, colTitles);
		} else {
//			LOGGER.debug("Storing CSV file from path : {}", csvFilePath);
			success = fuseki.storeCsv(csvFilePath, encoding, namespace);
		}
		LOGGER.debug(success ? "Stored CSV file - SUCCESS" : "Failed to store CSV file - ERROR");
		return RepeatStatus.FINISHED;
	}
}
