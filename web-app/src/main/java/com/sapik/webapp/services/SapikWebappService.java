package com.sapik.webapp.services;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobParameter;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.NoSuchJobException;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sapik.webapp.domain.IFusekiConnector;

/**
 * /**
 * <p>
 * Facade for <code>{@link SapikWebappApplication}</code>.
 * </p>
 */
@Service
public class SapikWebappService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SapikWebappService.class);

	@Autowired
	private JobRegistry jobRegistry;

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	private JobExplorer jobExplorer;

	@Autowired
	private IFusekiConnector fuseki;

	/**
	 * TripPlannerService constructor. Initializes the YAML reader
	 * 
	 * @param aYamlRepository
	 */
	public SapikWebappService() {
		LOGGER.debug("SapikWebappService created !");
	}

	public boolean dataLoaded() {
		return jobExplorer.findRunningJobExecutions("csvFileProcessJob").size() == 0;
	}

	public void saveFile(String filePath, String encoding, String url, String csvNamespace) {
		JobParameter csvFilePath = new JobParameter(filePath);
		JobParameter encode = new JobParameter(encoding);
		JobParameter namespace = new JobParameter(csvNamespace);
		Map<String, JobParameter> parameters = new HashMap<String, JobParameter>();
		parameters.put("csvFilePath", csvFilePath);
		parameters.put("encoding", encode);
		parameters.put("namespace", namespace);
//		LOGGER.debug("Launching batch process for csv file from path \"{}\"", filePath);

		try {
			jobLauncher.run(jobRegistry.getJob("csvFileProcessJob"), new JobParameters(parameters));
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
				| JobParametersInvalidException | NoSuchJobException e) {
			LOGGER.error("Error during batch process : {}", e);
			e.printStackTrace();
		}
	}

	public void saveFile(String filePath, String encoding, String url, String csvNamespace, String categories, String colTitles) {
		JobParameter csvFilePath = new JobParameter(filePath);
		JobParameter encode = new JobParameter(encoding);
		JobParameter namespace = new JobParameter(csvNamespace);
		JobParameter cat = new JobParameter(categories);
		JobParameter colTitle = new JobParameter(colTitles);
		Map<String, JobParameter> parameters = new HashMap<String, JobParameter>();
		parameters.put("csvFilePath", csvFilePath);
		parameters.put("encoding", encode);
		parameters.put("namespace", namespace);
		parameters.put("categories", cat);
		parameters.put("colTitles", colTitle);
		LOGGER.debug("Launching batch process for csv file from path \"{}\"", filePath);

		try {
			jobLauncher.run(jobRegistry.getJob("csvFileProcessJob"), new JobParameters(parameters));
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
				| JobParametersInvalidException | NoSuchJobException e) {
			LOGGER.error("Error during batch process : {}", e);
			e.printStackTrace();
		}
	}

	public String getAllDoctors() {
		return fuseki.getAll("doctors");
	}

	public String getDoctor(String id) {
		return fuseki.get("doctors", id);
	}

	public String getAllInstitutions() {
		return fuseki.getAll("institutions");
	}

	public String getFacility(String id) {
		return fuseki.get("institutions", id);
	}

	public String getInstitutionsInArea(String xMin, String xMax, String yMin, String yMax) {
		return fuseki.getInstitutionsInArea(xMin, xMax, yMin, yMax);
	}

	public String getInstitutionsInAreaForSpec(String spec, String xMin, String xMax, String yMin, String yMax) {
		return fuseki.getInstitutionsInAreaForSpec(spec, xMin, xMax, yMin, yMax);
	}
}
