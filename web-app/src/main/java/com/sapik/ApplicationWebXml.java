package com.sapik;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import com.sapik.webapp.config.BootProfiles;

/**
 * Helper Java class to create a web.xml.
 */
public class ApplicationWebXml extends SpringBootServletInitializer {

    private final Logger log = LoggerFactory.getLogger(ApplicationWebXml.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        // @formatter:off
        return application
                .profiles(addDefaultProfile())
                .sources(SapikWebApplication.class);
        // @formatter:on
    }

    /**
     * Set a default profile if it has not been set.
     * <p/>
     * <p>
     * Please use -Dspring.profiles.active=dev
     * </p>
     */
    private String addDefaultProfile() {
        String profile = System.getProperty("spring.profiles.active");
        if (profile == null) {
            profile = BootProfiles.DEV.toString();
        }
        log.info("Running with Spring profile(s) : {}", profile);
        return profile;
    }
}
