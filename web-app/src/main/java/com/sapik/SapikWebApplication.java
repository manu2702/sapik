package com.sapik;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SimpleCommandLinePropertySource;

import com.sapik.webapp.config.BootProfiles;
import com.sapik.webapp.domain.impl.FusekiEmbeddedServer;

@SpringBootApplication
public class SapikWebApplication {

    private static final Logger LOG = LoggerFactory.getLogger(SapikWebApplication.class);
    
    @Autowired
    private static FusekiEmbeddedServer fuseki;

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(SapikWebApplication.class);
        setDefaultProfile(app, args);

        Environment env = app.run(args).getEnvironment();
        fuseki = new FusekiEmbeddedServer(args, env.getProperty("sapik.webapp.csv-predicates").split(","));
        fuseki.start();
        LOG.info(
                new StringBuilder("\n")//
                        .append("-------------------------------------------------------------------------\n")//
                        .append("!! Sapik Web Application started. Listening on http://{}:{}   !!\n")//
                        .append("-------------------------------------------------------------------------")//
                        .toString(), env.getProperty("server.host", "localhost"),
                env.getProperty("server.port"));
        try {
			while(!fuseki.isStarted()){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					LOG.warn("Problem when trying to pause main thread : {}", e);
				}
			}
		} catch (IOException e) {
			LOG.warn("Problem with ping on fuseki : {}", e);
		}
    }

    private static void setDefaultProfile(SpringApplication app, String[] args) {
        SimpleCommandLinePropertySource source = new SimpleCommandLinePropertySource(args);
        if (!source.containsProperty("spring.profiles.active")) {
            app.setAdditionalProfiles(BootProfiles.DEV.toString());
        }
    }
}
