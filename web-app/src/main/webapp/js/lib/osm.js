var lon = -0.4644;
var lat = 52.1295;
var zoom = 13;
var lonLat;
var map;
function slippymap_resetPosition() {
	map.setCenter(lonLat, zoom);
}
function slippymap_getWikicode() {
	LL = map.getCenter().transform(map.getProjectionObject(),
			new OpenLayers.Projection("EPSG:4326"));
	Z = map.getZoom();
	size = map.getSize();
	prompt("Wikicode for this map view:", "<slippymap h=" + size.h + " w="
			+ size.w + " z=" + Z + " lat=" + LL.lat + " lon=" + LL.lon
			+ " layer=mapnik />");
}
function slippymap_init() {
	map = new OpenLayers.Map("map", {
		controls : [ new OpenLayers.Control.Navigation(),
				new OpenLayers.Control.PanZoomBar(),
				new OpenLayers.Control.Attribution() ],
		maxExtent : new OpenLayers.Bounds(-20037508.34, -20037508.34,
				20037508.34, 20037508.34),
		maxResolution : 156543.0399,
		units : 'meters',
		projection : "EPSG:900913"
	});
	layer = new OpenLayers.Layer.OSM.Mapnik("Mapnik");
	map.addLayer(layer);
	epsg4326 = new OpenLayers.Projection("EPSG:4326");
	lonLat = new OpenLayers.LonLat(lon, lat).transform(epsg4326, map
			.getProjectionObject());
	map.setCenter(lonLat, zoom);
	var getWikiCodeButton = new OpenLayers.Control.Button({
		title : "Get wikicode",
		displayClass : "getWikiCodeButton",
		trigger : slippymap_getWikicode
	});
	var resetButton = new OpenLayers.Control.Button({
		title : "Reset view",
		displayClass : "resetButton",
		trigger : slippymap_resetPosition
	});
	var panel = new OpenLayers.Control.Panel({
		displayClass : "buttonsPanel"
	});
	panel.addControls([ getWikiCodeButton, resetButton ]);
	map.addControl(panel);
}
slippymap_init();