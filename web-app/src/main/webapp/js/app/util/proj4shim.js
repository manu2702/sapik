/**
 * Defining OpenLayers ol variable globally (see https://github.com/requirejs/requirejs/issues/1400)
 */
define(['proj4'], function (proj4) {
  window.proj4 = proj4;
});