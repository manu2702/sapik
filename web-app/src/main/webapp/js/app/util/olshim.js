/**
 * Defining OpenLayers ol variable globally (see https://github.com/requirejs/requirejs/issues/1400)
 */
define(['ol'], function (ol) {
  window.ol = ol;
});