define( ['require', '../common-config'],
  function( require ) {
	require(['underscore', 'jquery'],
	function(_, $) {
/**
 * Elements that make up the popup.
 */
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');
var closer = document.getElementById('popup-closer');


/**
 * France GeoJSON
 * 
 * Layers for cities, departments and regions of France
 */

//var vectorRegions = new ol.layer.Vector({
//	title: 'regionsFrance',
//    source: new ol.source.Vector({
//        projection : 'EPSG:3857',
//        url: '/images/geojson/regions.geojson',
//        format: new ol.format.GeoJSON()
//     }),
//     maxZoomLevel: 7
//});
//var vectorDepartements = new ol.layer.Vector({
//	title: 'departementsFrance',
//    source: new ol.source.Vector({
//        projection : 'EPSG:3857',
//        url: '/images/geojson/departements.geojson',
//        format: new ol.format.GeoJSON()
//    }),
//    minZoomLevel: 7,
//    maxZoomLevel: 13
//});
//var vectorCommunes = new ol.layer.Vector({
//	title: 'communesFrance',
//    source: new ol.source.Vector({
//        projection : 'EPSG:3857',
//        url: '/images/geojson/communes.geojson',
//        format: new ol.format.GeoJSON()
//    }),
//	minZoomLevel: 13
//});


/**
 * Setting proj4js lib for the french coordinates projection, in order to use it in OpenLayers (ESPG 2154 - see http://spatialreference.org/ref/epsg/rgf93-lambert-93/)
 */
	ol.proj.setProj4(proj4);
	proj4.defs("EPSG:2154", "+proj=lcc +lat_1=49 +lat_2=44 +lat_0=46.5 +lon_0=3 +x_0=700000 +y_0=6600000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs");
	ol.proj.get("EPSG:2154").setExtent([-378305.81, 6093283.21, 1212610.74, 7186901.68]);

/**
 * We declare the source of the vector globally in order to be able to handle its content (pop-up and markers)
 */
var vectorSourceInstitutions = new ol.source.Vector({});
var vectorInstitutions = new ol.layer.Vector({
	title: 'medicalInstitutionsFrance',
    source: vectorSourceInstitutions
});

// style of the markers
var iconStyle = new ol.style.Style({
	  image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
	    anchor: [0.5, 46],
	    anchorXUnits: 'fraction',
	    anchorYUnits: 'pixels',
	    opacity: 0.5,
	    src: '/images/medicine.png'
	  }))
	});

/**
 * Add a click handler to hide the popup.
 * @return {boolean} Don't follow the href.
 */
closer.onclick = function() {
  overlay.setPosition(undefined);
  closer.blur();
  return false;
};

/**
 * Create an overlay to anchor the popup to the map.
 */
var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
  element: container,
  autoPan: true,
  autoPanAnimation: {
    duration: 250
  }
}));
var layer =  [ new ol.layer.Tile({
	source : new ol.source.OSM() // or MapQuest({ layer : 'sat' })
}), vectorInstitutions];
var map = new ol.Map({
	layers : layer,
	overlays: [overlay],
	target : 'map',
	controls : ol.control.defaults({
		attributionOptions : /** @type {olx.control.AttributionOptions} */
		({
			collapsible : false
		})
	}),
	view : new ol.View({ // 13/48.8570/2.3394
		center : ol.proj.fromLonLat([ 2.3488, 48.85341 ]),
		zoom : 7,
		//projection: frenchProjection
	})
});
//var changeProjection = function() {
//	  var zoomLevel = map.getView().getZoom();
//
//	  // look if zoom level is reached
//	  if (zoomLevel > 11) {
//		  map.removeLayer(vectorRegions);
//		  map.removeLayer(vectorDepartements);
//		  map.removeLayer(vectorCommunes);
//		  map.addLayer(vectorCommunes);
//	  } else if (zoomLevel > 8 && zoomLevel <= 11) {
//		  map.removeLayer(vectorRegions);
//		  map.removeLayer(vectorCommunes);
//		  map.removeLayer(vectorDepartements);
//		  map.addLayer(vectorDepartements);
//	  } else if (zoomLevel <= 8) {
//		  map.removeLayer(vectorCommunes);
//		  map.removeLayer(vectorDepartements);
//		  map.removeLayer(vectorRegions);
//		  map.addLayer(vectorRegions);
//	  }
//};
function getRegionExtent() {
	var zoomLevel = map.getView().getZoom();

	  // look if zoom level is reached
	  if (zoomLevel > 11) {
		  return vectorRegions.getSource().getExtent();
	  } else if (zoomLevel > 8 && zoomLevel <= 11) {
		  return vectorDepartements.getSource().getExtent();
	  } else if (zoomLevel <= 8) {
		  return vectorCommunes.getSource().getExtent();
	  }
}
function selected (evt) {
    map.zoomToExtent(evt.feature.geometry.getBounds(), closest=true);
}

//control.activate();
var target = $(map.getTargetElement()); //getTargetElement is experimental as of 01.10.2015
map.on('pointermove', function (evt) {
    if (map.hasFeatureAtPixel(evt.pixel)) { //hasFeatureAtPixel is experimental as of 01.10.2015
        target.css('cursor', 'pointer');
    } else {
        target.css('cursor', '');
    }
});


//var selectMouseMove = new ol.interaction.Select({
//	  condition: ol.events.condition.mouseMove
//	});
//map.addInteraction(selectMouseMove);

//used to change layer depending on zoom
//map.getView().on('change:resolution', changeProjection);
/**
 * Add a click handler to the map to render the popup.
 */
map.on('singleclick', function(evt) {
  var coordinate = evt.coordinate;
  var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
      coordinate, 'EPSG:3857', 'EPSG:4326'));
  overlay.setPosition(coordinate);
  
  var feature = map.forEachFeatureAtPixel(evt.pixel,
	      function(feature, layer) {
	  		//map.getView().setCenter(map.getCoordinateFromPixel(evt.pixel), map.getView().getZoom());
	        return feature;
	      });
	    if (feature) {
	        var geometry = feature.getGeometry();
	        var coord = geometry.getCoordinates();
	        
	        $nom = (feature.get('nomLong') == 'null' || feature.get('nomLong')=='') ? feature.get('name') : feature.get('nomLong');
	        var con = '<p>' + $nom + '</p>'
	        + '<code>Spécialité : ' + feature.get('objectD') + '</code></br>'
	        + '<code>Finess : ' + feature.get('finess') + '</code></br>'
	        + '<code>Addresse : ' + feature.get('numVoie') + ' ' + feature.get('typVoie') + ' ' + feature.get('voie') + ' ' + feature.get('compVoie') + '</br>' + feature.get('lieuDitBp')
	        + ' ' + feature.get('commune') + ' ' + feature.get('departement') + ' ' + feature.get('libDepartement') + '</code></br>';
	        con += '<code>Tel : ' + feature.get('tel') + '</code></br>';
	        con += '<code>Fax : ' + feature.get('fax') + '</code>';
	        
	        content.innerHTML = con;
	    }
});

var resizeMap = function() {
	var div = $('#map');
	div.height($(window).height() - $('nav').height() - 2);
	map.updateSize();
}
resizeMap();

window.onresize = resizeMap;

// Method for getting responsive svg and add links to svg elements
var setBodyListener = function (svgBody) {
	$('#' + svgBody).on('load', function() {
		$(this).attr("href","#");
	});
	$('#' + svgBody).on('mouseover', function() {
		$(this).css("opacity", "1");
		if (svgBody == "manSvg") {
			$(this).css("fill", "#dd4d3b");
		} else {
			$(this).css("fill", "#c455a7");
		}
		$(this).css("cursor", "pointer");
	});
	$('#' + svgBody).on('mouseout', function() {
		$(this).css("opacity", "0.4");
		if (svgBody == "manSvg") {
			$(this).css("fill", "#B32F1F");
		} else {
			$(this).css("fill", "#99357F");
		}
		$(this).css("cursor", "auto");
	});
	$('#' + svgBody).on('click', function(event) {
		$currentExtent = map.getView().calculateExtent(map.getSize());
		$viewCoordinates = ol.proj.transformExtent($currentExtent, 'EPSG:3857', 'EPSG:2154');
		$tmpStr = event.target.id;
		$.get("http://localhost:8888/api/institutions/geoloc",
				{
					xMin: $viewCoordinates[0].toFixed(2),
					xMax: $viewCoordinates[2].toFixed(2),
					yMin: $viewCoordinates[1].toFixed(2),
					yMax: $viewCoordinates[3].toFixed(2)
				},
				function(response) {
					vectorSourceInstitutions.clear();
					
					$.each(response.results.bindings, function (index, binding) {
						var coord = new ol.geom.Point([Number(binding.coordX.value), Number(binding.coordY.value)]);							
						var customFeature = new ol.Feature({
						    name: binding.nom.value,
						    geometry: coord,
						    finess: binding.finess.value,
						    coordX: binding.coordX.value,
						    coordY: binding.coordY.value,
						    nomLong: binding.nomLong.value,
						    numVoie: binding.numVoie.value,
						    typVoie: binding.typVoie.value,
						    voie: binding.voie.value,
						    compVoie: binding.compVoie.value,
						    lieuDitBp: binding.lieuDitBp.value,
						    commune: binding.commune.value,
						    departement: binding.departement.value,
						    libDepartement: binding.libDepartement.value,
						    ligneAcheminement: binding.ligneAcheminement.value,
						    tel: binding.tel.value,
						    fax: binding.fax.value
						});
						customFeature.setStyle(iconStyle);
						customFeature.getGeometry().transform("EPSG:2154", "EPSG:3857");
						vectorSourceInstitutions.addFeature(customFeature);
					});
				}, "json");
	});
}
$(document).ready( function() {
$('#welcome').html(
	'</br><h1> Zoomez d\'abord sur vore zone de recherche <i class="fa fa-toggle-right"/></h1></br></br></br></br><h1><i class="fa fa-toggle-down"/> Puis choisissez la spécialité <i class="fa fa-toggle-down"/></h1></br></br>'
		+ '<ul><li>Cliquez sur le corps pour voir tous les établissements de la zone</li><li>Cliquez sur une partie du corps pour filtrer les résultats par spécialité</li></ul>');
$('#welcome').css("text-align", "center");
$('#buttonsBar').attr("class", "nav nav-tabs");
$('#buttonsBar').html('<li role="presentation" class="active"><a href="#">Animation</a></li>'
						+ '<li role="presentation"><a href="#">Formulaire</a></li>'
						+ '<li><button id="resetBodies" class="btn btn-default">Reset</button></li>');
$('.navbar').css("margin-bottom", "0px");
$('#sidebar').css("padding-left", "0px");
$('#sidebar').css("position", "fixed");
$('#sidebar').css("bottom", "0");
$('#mainContent').css("margin-left", $('#sidebar').width());
//$('#sidebar').css("padding-right", "0px");
$('#mainContent').css("padding-right", "0px");
$('#resetBodies').click(function () {
	$('#man').css('display', 'block');
	$('#woman').css('display', 'block');
	resetView();
	resizeMap();
});
$('#svgImage').load('/images/svg/humanBodies.svg', function() {
	$svgElements = [ "manHead", "manShoulders", "manKnees", "manLungs", "manKidneys",
					 "manFeet", "manStomach", "manShins"];
	for (var i = 0; i < $svgElements.length; i++) {
		$('#' + $svgElements[i]).on('load', function() {
			$(this).attr("href", "#");
		});
		$('#' + $svgElements[i]).on('mouseover', function() {
			$(this).css("opacity", "0.8");
			$(this).css("cursor", "pointer");
		});
		$('#' + $svgElements[i]).on('mouseout', function() {
			$(this).css("opacity", "0.4");
			$(this).css("cursor", "auto");
		});
		$('#' + $svgElements[i]).on('click', function(event) {
			$currentExtent = map.getView().calculateExtent(map.getSize());
			$viewCoordinates = ol.proj.transformExtent($currentExtent, 'EPSG:3857', 'EPSG:2154');
			$tmpStr = event.target.id;
			$.get("http://localhost:8888/api/institutions/geolocalisation/" + $tmpStr.replace("man","").toLowerCase(),
					{
						xMin: $viewCoordinates[0].toFixed(2),
						xMax: $viewCoordinates[2].toFixed(2),
						yMin: $viewCoordinates[1].toFixed(2),
						yMax: $viewCoordinates[3].toFixed(2)
					},
					function(response) {
						vectorSourceInstitutions.clear();
						//vectorInstitutions.destroyFeatures();
						
						$.each(response.results.bindings, function (index, binding) {
							var coord = new ol.geom.Point([Number(binding.coordX.value), Number(binding.coordY.value)]);							
							var customFeature = new ol.Feature({
							    name: binding.nom.value,
							    geometry: coord,
							    finess: binding.finess.value,
							    coordX: binding.coordX.value,
							    coordY: binding.coordY.value,
							    nomLong: binding.nomLong.value,
							    numVoie: binding.numVoie.value,
							    typVoie: binding.typVoie.value,
							    voie: binding.voie.value,
							    compVoie: binding.compVoie.value,
							    lieuDitBp: binding.lieuDitBp.value,
							    commune: binding.commune.value,
							    departement: binding.departement.value,
							    libDepartement: binding.libDepartement.value,
							    ligneAcheminement: binding.ligneAcheminement.value,
							    tel: binding.tel.value,
							    fax: binding.fax.value,
							    objectD: binding.objectD.value
							});
							customFeature.setStyle(iconStyle);
							customFeature.getGeometry().transform("EPSG:2154", "EPSG:3857");
							vectorSourceInstitutions.addFeature(customFeature);
						});
					}, "json");
		});
	}
	setBodyListener("manSvg");
	$svgElements = [ "womanHead", "womanShoulders",
					 "womanKnees", "womanLungs", "womanKidneys", "womanFeet", "womanStomach",
					 "womanShins" ];
	for (var i = 0; i < $svgElements.length; i++) {
		$('#' + $svgElements[i]).on('load', function() {
			$(this).attr("href", "#");
		});
		$('#' + $svgElements[i]).on('mouseover', function() {
			$(this).css("opacity", "0.8");
			$(this).css("cursor", "pointer");
		});
		$('#' + $svgElements[i]).on('mouseout', function() {
			$(this).css("opacity", "0.4");
			$(this).css("cursor", "auto");
		});
		$('#' + $svgElements[i]).on('click', function(event) {
			$currentExtent = map.getView().calculateExtent(map.getSize());
			$viewCoordinates = ol.proj.transformExtent($currentExtent, 'EPSG:3857', 'EPSG:2154');
			$tmpStr = event.target.id;
			$requestUri = "";
			if($tmpStr == "womanKidneys") {
				$requestUri = "http://localhost:8888/api/institutions/geolocalisation/womankidneys";
			} else {
				$requestUri = "http://localhost:8888/api/institutions/geolocalisation/" + $tmpStr.replace("woman","").toLowerCase();
			}
			$.get($requestUri,
					{
						xMin: $viewCoordinates[0].toFixed(2),
						xMax: $viewCoordinates[2].toFixed(2),
						yMin: $viewCoordinates[1].toFixed(2),
						yMax: $viewCoordinates[3].toFixed(2)
					},
					function(response) {
						vectorSourceInstitutions.clear();
						
						$.each(response.results.bindings, function (index, binding) {
							var coord = new ol.geom.Point([Number(binding.coordX.value), Number(binding.coordY.value)]);							
							var customFeature = new ol.Feature({
							    name: binding.nom.value,
							    geometry: coord,
							    finess: binding.finess.value,
							    coordX: binding.coordX.value,
							    coordY: binding.coordY.value,
							    nomLong: binding.nomLong.value,
							    numVoie: binding.numVoie.value,
							    typVoie: binding.typVoie.value,
							    voie: binding.voie.value,
							    compVoie: binding.compVoie.value,
							    lieuDitBp: binding.lieuDitBp.value,
							    commune: binding.commune.value,
							    departement: binding.departement.value,
							    libDepartement: binding.libDepartement.value,
							    ligneAcheminement: binding.ligneAcheminement.value,
							    tel: binding.tel.value,
							    fax: binding.fax.value,
							    objectD: binding.objectD.value
							});
							customFeature.setStyle(iconStyle);
							customFeature.getGeometry().transform("EPSG:2154", "EPSG:3857");
							vectorSourceInstitutions.addFeature(customFeature);
						});
					}, "json");
		});
	}
	setBodyListener("womanSvg");
});
});



});
});
    